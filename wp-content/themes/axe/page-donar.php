<?php
/* Template Name: Donar */

if ( isset( $_POST['submit'] ) ) {
	$tuCuotaToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNWE1ODBmM2U4MGM4MzVlNDIxNmE3MTUwYmQ2OTVjNzRjM2I2NDFlNjk0MWNiMWY0MDIxNDQ1ZDFmNTZkNTdjZDdjZGJlN2M3OWE2MDI4M2IiLCJpYXQiOjE2MzcyNTAxNzIuNTEyNTI3LCJuYmYiOjE2MzcyNTAxNzIuNTEyNTM1LCJleHAiOjE2Njg3ODYxNzIuNTA0ODQsInN1YiI6Ijc0Iiwic2NvcGVzIjpbXX0.N6fm7H6yjvPV3KCzlxF_LhF4S8fFHOfNmoag2W3W4IU2fZJq3AnMfmly2PI315OFtpngDy4qYvCsT1U8gdgXsUFS2_hOzykvbdQQW9gO9fDS2VJyfhuE0ETJmhBS7EOWYpxJR2p-7XH1_Ly86lmTZopWSwaXDW88SZFEXlSzR2DcSj3r8MoUj19pkKxMv-6OwN7zMMNX2cSJvo4iXDi9B7fw4ofNRe6hQGaCwpUdn4X7QNVd7dHyUhVjCm8p4vrkHER4za-gzZlXkj2Rp7g2HflbBS5InZO0VrOyT51bC98kABoTjLq8-CZlwOVfuY3-rkykK8gdMSbubLuG5UF3sw-rzbfqTo262V981fqRgjJCLcT97YGBI_7EatSQflwZGdslf4fQw-Bt-FzCak0J08K4tteGS3-HILXWohwOzt3HQQXqzuQayyBFagknoEcpFg8WTHVa6rQqLPt0cpsTwVf2d_CAfbHz5-Tev3Uw6asr-t5sY7jrhdA8F5tg5Dcg6nIDEw3qpTHKZjB4eh8Mwb3RTIYhWaAq028pghy-QhJil0ge_7hZ2IBP4umzH7CriunBmQrRnECMJpbLSRwWDceSjgpGg2KrvEAogonBozkrBhDbZ70N1jWzwAwEwjxi15deBjO2TEjnvQgwuci50Qc8iPhRxuNPizjYlbU977g";
	$endpoint     = 'https://sandbox.tucuota.com/api/sessions';
	$name         = $_POST['nombre'] . ' ' . $_POST['apellido'];
	if ( is_numeric( $_POST['amount'] ) ) {
		$amount = $_POST['amount'];
	} else {
		$amount = '1';
	}
	if ( $amount > 1 ) {
		$editable_amount = false;
	} else {
		$editable_amount = true;
	}
	if ( $amount > 1 ) {
		$description = round( $amount / 30 ) . ' pesos por día';
	} else {
		$description = 'Gracias ' . $name . ' por tu aporte.';
	}

	$post_data = array(
		'kind'           => 'subscription',
		'interval_unit'  => "monthly",
		'success_url'    => 'https://argentinosporlaeducacion.org',
		'customer_name'  => $name,
		'customer_email' => $_POST['email'],
		'amount'         => $amount,
		'description'    => $description,
	);

	if ( $amount < 2 ) {
		$post_data['editable_amount'] = true;
	}


	$headers = [
		'Authorization: Bearer ' . $tuCuotaToken
	];


	// Prepare data
	$ch = curl_init( $endpoint );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLINFO_HEADER_OUT, true );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_data );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

	// Api call
	$response     = curl_exec( $ch );
	$responseCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

	// close connection
	curl_close( $ch );

	$response = json_decode( $response, true );

	if ( $responseCode >= 400 ) {
		echo "<pre>" . print_r( $response ) . "</pre>";
	} else {
		$url = $response['data']['public_uri'];
		wp_redirect( $url );
		exit;
	}

} else {

	get_header();

	$pid              = get_the_ID();
	$featured_img_url = get_the_post_thumbnail_url( $pid, 'full' ); // Imagen

	$subtitulo   = get_field( 'subtitle' );
	$descripcion = get_field( 'description' );
	?>

	<div class="c-donar"<?php
	if ( ! empty( $featured_img_url ) ) {
		echo 'style="background:transparent;"';
	} ?>
	>
		<?php if ( $featured_img_url ) { ?>
			<div class="c-donar__featured-image">
				<div class="c-datos__featured-image-inner"
					 style="background-image:url(' <?php echo $featured_img_url; ?>');">
				</div>
			</div>
			<?php
		} ?>

		<header class="c-donar__header">
			<div class="c-donar__header-container o-container o-container-narrow">
				<div class="c-donar__header-inner">
					<?php the_title( '<h1 class="c-donar__header-heading">', '</h1>' ); ?>
					<?php echo '<div class="c-donar__header-subtitle">' . $subtitulo . '</div>' ?>

					<div class="c-donar__header-grid">
						<form method="POST" class="c-donar__form c-donar__form">
							<div class="c-donar__form-nombre">
								<label for="nombre">Nombre</label>
								<input type="text" id="nombre" name="nombre" required>
							</div>
							<div class="c-donar__form-apellido">
								<label for="apellido">Apellido</label>
								<input type="text" id="apellido" name="apellido" required>
							</div>
							<div class="c-donar__form-email">
								<label for="email">Email</label>
								<input id="email" name="email" type="email" required>
							</div>
							<fieldset class="c-donar__form-amount">
								<?php

								// Check rows exists.
								$frontpage_id = get_option( 'page_on_front' );
								if ( have_rows( 'donar_call-to-actions', $frontpage_id ) ):
									echo '<fieldset class="c-donar__form-amount">';
									// Loop through rows.
									while ( have_rows( 'donar_call-to-actions', $frontpage_id ) ) : the_row();

										// Load sub field value.
										$donar_etiqueta    = get_sub_field( 'donar_etiqueta' );
										$donar_descripcion = get_sub_field( 'donar_descripcion' );
										$donar_prefix      = get_sub_field( 'donar_prefix' );
										$donar_suffix      = get_sub_field( 'donar_suffix' );
										echo '<input type="radio" id="amount-' . get_row_index() . '" name="amount" value="';
										if ( is_numeric( $donar_etiqueta ) ) {
											echo $donar_etiqueta;
										} else {
											echo '1';
										}
										echo '"';

										if ( get_row_index() == 1 ) {
											echo 'checked';
										};
										echo '/>';
										echo '<label for="amount-' . get_row_index() . '">';
										if ( $donar_prefix ) {
											echo '<span class="c-donar__form-amount-prefix">' . $donar_prefix . '</span>';
										}
										echo '<span class="c-donar__form-amount-number">' . $donar_etiqueta . '</span>';
										if ( $donar_suffix ) {
											echo '<span class="c-donar__form-amount-suffix">' . $donar_suffix . '</span>';
										}
										echo '<span class="c-donar__form-amount-descripcion">' . $donar_descripcion . '</span></label>';
									endwhile;
									echo '</fieldset>';
								endif;
								?>
							</fieldset>
							<div class="c-donar__form-button">
								<input type="submit" name="submit" id="submit" value="Donar"
									   class="c-donar__form-field o-button__block">
							</div>
						</form>
						<?php echo '<div class="c-donar__header-subtitle">' . $descripcion . '</div>' ?>
					</div>


				</div>
			</div>
		</header>


	</div>

	<?php

	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			?>
			<?php the_content(); ?>
			<?php
		}
	}

}

get_footer();
