<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package AxE
 */

get_header();
$pid  = get_the_ID();
$size = 'full'; // (thumbnail, medium, large, full or custom size)

$subtitulo   = get_field( 'subtitle' );
$description = get_field( 'description' );

$referentes_titulo      = get_field( 'referentes_titulo', $pid );
$referentes_descripcion = get_field( 'referentes_descripcion', $pid );
$referentes             = get_field( 'referentes_texto', $pid );

?>
	<header class="entry-header">
		<div class="o-container o-container-narrow">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php echo '<div class="page-subtitle">' . $subtitulo . '</div>' ?>
		</div>
	</header><!-- .entry-header -->
	<!--Test-->

<?php
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		echo '<div class="o-container o-container-narrow page-description">';
		echo $description;
		echo '</div>';
	}
}

?>


<?php
wp_archive_staff( 'consejo-asesor', 6 );
?>


<?php if ( get_field( 'video_oembed' ) ) { ?>
	<section class="c-video o-section">
		<div class="o-container c-video__inner">
			<?php
			get_template_part( 'template-parts/components/video' );
			?>
		</div>
	</section>
<?php } ?>


<?php
wp_archive_staff( 'equipo', - 1 );
?>


	<section class="c-referentes o-section">
		<div class="c-referentes__container o-container">

			<?php if ( ! empty( $referentes_titulo ) ) {
				echo '<div class="c-referentes__title">';
				echo esc_html( $referentes_titulo );
				echo '</div>';
			} ?>
			<?php if ( ! empty( $referentes_descripcion ) ) {
				echo '<div class="c-referentes__descripcion">';
				echo esc_html( $referentes_descripcion );
				echo '</div>';
			} ?>
			<?php if ( ! empty( $referentes ) ) {
				echo '<div class="c-referentes__content">';
				echo esc_html( $referentes );
				echo '</div>';
			} ?>
		</div>
	</section>

<?php
wp_archive_staff( 'ex-ministros', - 1 );
?>


<?php
$logos_organizaciones = get_field( 'logos_organizaciones' );
if ( $logos_organizaciones ): ?>
	<div class="c-logo-block o-section">
		<div class="c-logo-block__container o-container">
			<div class="c-quotes__heading"><?php echo $logos_organizaciones['titulo']; ?></div>
			<div class="c-logo-block__content">
				<img src="<?php echo esc_url( $logos_organizaciones['imagen']['url'] ); ?>"/>
			</div>
		</div>
	</div>
<?php endif; ?>


<?php if ( get_field( 'mapa_embed' ) ) { ?>
	<section class="c-mapa o-section">
		<div class="o-container c-mapa__inner">
			<?php
			get_template_part( 'template-parts/components/mapa' );
			?>
		</div>
	</section>
<?php } ?>


<?php
$logos_empresas = get_field( 'logos_empresas' );
if ( $logos_empresas ): ?>
	<div class="c-logo-block o-section">
		<div class="c-logo-block__container o-container">
			<div class="c-quotes__heading"><?php echo $logos_empresas['titulo']; ?></div>
			<div class="c-logo-block__content">
				<img src="<?php echo esc_url( $logos_empresas['imagen']['url'] ); ?>"/>
			</div>
		</div>
	</div>
<?php endif; ?>


<?php
if ( have_rows( 'quotes_repeater' ) ):
	echo '<section id="quotes" class="c-quotes">
		<div class="c-quotes__inner o-container">
			<div class="container">
				<div class="c-quotes__heading">Testimonios</div>
				<div class="c-quotes__slick slick single-item">';
	while ( have_rows( 'quotes_repeater' ) ) : the_row();
		get_template_part( 'template-parts/components/slide', 'quotes' );
	endwhile;

	echo '</div>
	</div>
	<div class="c-quotes__cta-wraper">
		<div class="c-quotes__cta-wraper__btn o-button">Sumate</div>
	</div>
	</div>
	<div class="c-shape__03">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1400 163.4">
			<path d="M.5,0S49,63,162.5,21.3s172.6,1,208.5,14.6,151.3-9.7,161.9-18.4c13.6-5.9,13.6,14.5,26.2,23.2,0,0,117.4,94.1,232.8,90.2s184.3-51.4,277.4-51.4,135.8,22.3,192,15.5S1400,42.7,1400,42.7l-.5,120.7H0Z"
				  style="fill:#fff"/>
		</svg>
	</div>
	</section>';
endif;
?>


<?php
get_template_part( 'template-parts/components/banner' );

get_footer();