<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package AxE
 */

get_header();

$subtitulo   = get_field( 'subtitle' );
$description = get_field( 'description' );

?>
	<header class="entry-header">
		<div class="o-container o-container-narrow">
			<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
			<?php echo '<div class="page-subtitle">' . $subtitulo . '</div>' ?>
		</div>
	</header><!-- .entry-header -->

<?php
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		echo '<div class="o-container o-container-narrow page-description">';
		echo $description;


		get_template_part( 'template-parts/components/features' );

		echo '</div>';
	}
}
?>


	<div class="line"></div>
<?php
get_footer();
