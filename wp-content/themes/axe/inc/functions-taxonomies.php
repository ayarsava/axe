<?php
/**
 * Declare custom taxonomies in this file!
 */

/**
 * Register taxonomies and create default terms.
 */
function axe_register_tax() {
	create_acciones_taxonomy();
	create_areas_taxonomy();
	create_provincias_taxonomy();
	create_niveles_taxonomy();
	create_rol_taxonomy();

	// Subtypes
	//create_event_subtype_taxonomy();
	//create_organisation_subtype_taxonomy();
	//create_resource_subtype_taxonomy();

	// Create Default Taxonomy Terms.
	//axe_add_default_terms();

	// Reset.
	// update_option( 'initial_taxes_created', false );
}

add_action( 'init', 'axe_register_tax', 0 );


/**
 * Register tipos de campaña taxonomy (hierarchical like categories)
 */
function create_acciones_taxonomy() {
	$plural   = __( 'Acciones', 'text_domain' );
	$singular = __( 'Acción', 'text_domain' );

	// Set label names.
	// phpcs:disable
	$labels = array(
		'name'              => $plural,
		'singular_name'     => $singular,
		'search_items'      => sprintf( __( 'Search %s', 'text_domain' ), $plural ),
		'all_items'         => sprintf( __( 'All %s', 'text_domain' ), $plural ),
		'parent_item'       => sprintf( __( 'Parent %s', 'text_domain' ), $singular ),
		'parent_item_colon' => sprintf( __( 'Parent %s:', 'text_domain' ), $plural ),
		'edit_item'         => sprintf( __( 'Edit %s', 'text_domain' ), $singular ),
		'update_item'       => sprintf( __( 'Update %s', 'text_domain' ), $singular ),
		'add_new_item'      => sprintf( __( 'Add New %s', 'text_domain' ), $singular ),
		'new_item_name'     => sprintf( __( 'New %s Name', 'text_domain' ), $singular ),
		'menu_name'         => $plural,
	);
	// phpcs:enable

	// Register the taxonomy.
	register_taxonomy(
		sanitize_title( strtolower( $plural ) ),
		array(
			'campana',
			'post',
		),
		array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			// 'show_in_rest' =>true, // Uncomment for Gutenberg.
			'query_var'         => true,
			'rewrite'           => array( 'slug' => sanitize_title( strtolower( $singular ) ) ),
		)
	);
}

/**
 * Register areas taxonomy (hierarchical like categories)
 */
function create_areas_taxonomy() {
	$plural   = __( 'Areas', 'text_domain' );
	$singular = __( 'Area', 'text_domain' );

	// Set label names.
	// phpcs:disable
	$labels = array(
		'name'              => $plural,
		'singular_name'     => $singular,
		'search_items'      => sprintf( __( 'Search %s', 'text_domain' ), $plural ),
		'all_items'         => sprintf( __( 'All %s', 'text_domain' ), $plural ),
		'parent_item'       => sprintf( __( 'Parent %s', 'text_domain' ), $singular ),
		'parent_item_colon' => sprintf( __( 'Parent %s:', 'text_domain' ), $plural ),
		'edit_item'         => sprintf( __( 'Edit %s', 'text_domain' ), $singular ),
		'update_item'       => sprintf( __( 'Update %s', 'text_domain' ), $singular ),
		'add_new_item'      => sprintf( __( 'Add New %s', 'text_domain' ), $singular ),
		'new_item_name'     => sprintf( __( 'New %s Name', 'text_domain' ), $singular ),
		'menu_name'         => $plural,
	);
	// phpcs:enable

	// Register the taxonomy.
	register_taxonomy(
		sanitize_title( strtolower( $plural ) ),
		array(
			'indicadores',
		),
		array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			// 'show_in_rest' =>true, // Uncomment for Gutenberg.
			'query_var'         => true,
			'rewrite'           => array( 'slug' => sanitize_title( strtolower( $singular ) ) ),
		)
	);
}

/**
 * Register provincias taxonomy (hierarchical like categories)
 */
function create_provincias_taxonomy() {
	$plural   = __( 'Provincias', 'text_domain' );
	$singular = __( 'Provincia', 'text_domain' );

	// Set label names.
	// phpcs:disable
	$labels = array(
		'name'              => $plural,
		'singular_name'     => $singular,
		'search_items'      => sprintf( __( 'Search %s', 'text_domain' ), $plural ),
		'all_items'         => sprintf( __( 'All %s', 'text_domain' ), $plural ),
		'parent_item'       => sprintf( __( 'Parent %s', 'text_domain' ), $singular ),
		'parent_item_colon' => sprintf( __( 'Parent %s:', 'text_domain' ), $plural ),
		'edit_item'         => sprintf( __( 'Edit %s', 'text_domain' ), $singular ),
		'update_item'       => sprintf( __( 'Update %s', 'text_domain' ), $singular ),
		'add_new_item'      => sprintf( __( 'Add New %s', 'text_domain' ), $singular ),
		'new_item_name'     => sprintf( __( 'New %s Name', 'text_domain' ), $singular ),
		'menu_name'         => $plural,
	);
	// phpcs:enable

	// Register the taxonomy.
	register_taxonomy(
		sanitize_title( strtolower( $plural ) ),
		array(
			'indicadores',
		),
		array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			// 'show_in_rest' =>true, // Uncomment for Gutenberg.
			'query_var'         => true,
			'rewrite'           => array( 'slug' => sanitize_title( strtolower( $singular ) ) ),
		)
	);
}

/**
 * Register niveles taxonomy (hierarchical like categories)
 */
function create_niveles_taxonomy() {
	$plural   = __( 'Niveles', 'text_domain' );
	$singular = __( 'Nivel', 'text_domain' );

	// Set label names.
	// phpcs:disable
	$labels = array(
		'name'              => $plural,
		'singular_name'     => $singular,
		'search_items'      => sprintf( __( 'Search %s', 'text_domain' ), $plural ),
		'all_items'         => sprintf( __( 'All %s', 'text_domain' ), $plural ),
		'parent_item'       => sprintf( __( 'Parent %s', 'text_domain' ), $singular ),
		'parent_item_colon' => sprintf( __( 'Parent %s:', 'text_domain' ), $plural ),
		'edit_item'         => sprintf( __( 'Edit %s', 'text_domain' ), $singular ),
		'update_item'       => sprintf( __( 'Update %s', 'text_domain' ), $singular ),
		'add_new_item'      => sprintf( __( 'Add New %s', 'text_domain' ), $singular ),
		'new_item_name'     => sprintf( __( 'New %s Name', 'text_domain' ), $singular ),
		'menu_name'         => $plural,
	);
	// phpcs:enable

	// Register the taxonomy.
	register_taxonomy(
		sanitize_title( strtolower( $plural ) ),
		array(
			'indicadores',
		),
		array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			// 'show_in_rest' =>true, // Uncomment for Gutenberg.
			'query_var'         => true,
			'rewrite'           => array( 'slug' => sanitize_title( strtolower( $singular ) ) ),
		)
	);
}


/**
 * Register rol taxonomy (hierarchical like categories)
 */
function create_rol_taxonomy() {
	$plural   = __( 'Roles', 'text_domain' );
	$singular = __( 'Rol', 'text_domain' );

	// Set label names.
	// phpcs:disable
	$labels = array(
		'name'              => $plural,
		'singular_name'     => $singular,
		'search_items'      => sprintf( __( 'Search %s', 'text_domain' ), $plural ),
		'all_items'         => sprintf( __( 'All %s', 'text_domain' ), $plural ),
		'parent_item'       => sprintf( __( 'Parent %s', 'text_domain' ), $singular ),
		'parent_item_colon' => sprintf( __( 'Parent %s:', 'text_domain' ), $plural ),
		'edit_item'         => sprintf( __( 'Edit %s', 'text_domain' ), $singular ),
		'update_item'       => sprintf( __( 'Update %s', 'text_domain' ), $singular ),
		'add_new_item'      => sprintf( __( 'Add New %s', 'text_domain' ), $singular ),
		'new_item_name'     => sprintf( __( 'New %s Name', 'text_domain' ), $singular ),
		'menu_name'         => $plural,
	);
	// phpcs:enable

	// Register the taxonomy.
	register_taxonomy(
		sanitize_title( strtolower( $plural ) ),
		array(
			'staff',
		),
		array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			// 'show_in_rest' =>true, // Uncomment for Gutenberg.
			'query_var'         => true,
			'rewrite'           => array( 'slug' => sanitize_title( strtolower( $singular ) ) ),
		)
	);
}

add_action( 'do_meta_boxes', 'wpdocs_remove_plugin_metaboxes' );

/**
 * Remove Editorial Flow meta box for users that cannot delete pages
 */
function wpdocs_remove_plugin_metaboxes() {
	remove_meta_box( 'provinciasdiv', array( 'campana', 'indicadores' ), 'side' );
	remove_meta_box( 'areasdiv', 'indicadores', 'side' );
	remove_meta_box( 'nivelesdiv', 'indicadores', 'side' );
	remove_meta_box( 'accionesdiv', array( 'campana', 'post' ), 'side' );
	remove_meta_box( 'categorydiv', array( 'post' ), 'side' );
}