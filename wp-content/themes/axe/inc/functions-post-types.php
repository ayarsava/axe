<?php
/**
 * Declare custom post types in this file!
 */


/**
 * Register the custom post types
 */
function axe_register_cpts() {

	cp_change_post_object();
	register_informes();
	register_indicadores();
	register_campanas();
	register_staff();
}

add_action( 'init', 'axe_register_cpts', 0 );


/**
 * Individual post type functions | Informes
 */
function register_informes() {
	$singular = 'Informe';
	$plural   = 'Informes';

	// Set label names for the WP backend UI.
	$labels = array(
		'name'               => _x( $plural, 'Post Type General Name', 'text_domain' ),
		'singular_name'      => _x( $singular, 'Post Type Singular Name', 'text_domain' ),
		'menu_name'          => __( $plural, 'text_domain' ),
		'parent_item_colon'  => __( 'Padre ' . $singular, 'text_domain' ),
		'all_items'          => __( 'Todos los ' . $plural, 'text_domain' ),
		'view_item'          => __( 'Ver ' . $singular, 'text_domain' ),
		'add_new_item'       => __( 'Agregar nuevo ' . $singular, 'text_domain' ),
		'add_new'            => __( 'Agregar nuevo', 'text_domain' ),
		'edit_item'          => __( 'Editar ' . $singular, 'text_domain' ),
		'update_item'        => __( 'Actualizar ' . $singular, 'text_domain' ),
		'search_items'       => __( 'Buscar ' . $singular, 'text_domain' ),
		'not_found'          => __( 'No hemos encontrado resultados', 'text_domain' ),
		'not_found_in_trash' => __( 'No hemos encontrado resultados en la papelera', 'text_domain' ),
	);

	// Set post type options.
	$args = array(
		'label'               => __( strtolower( $plural ), 'text_domain' ),
		'description'         => __( 'My list of ' . strtolower( $plural ), 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array(
			//'editor',
			//'excerpt',
			'revisions',
			'title',
			// phpcs:disable
			//'author',
			//'custom-fields',
			//'comments',
			//'page-attributes',
			//'post-formats',
			'thumbnail',
			// phpcs:enable
		),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		// Icons can be found at: https://developer.wordpress.org/resource/dashicons/.
		'menu_icon'           => 'dashicons-media-document',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		// 'show_in_rest' =>true, // Uncomment for Gutenberg.
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'rewrite'             => array(
			'slug'       => sanitize_title( strtolower( $singular ) ),
			'with_front' => false,
		),
	);
	register_post_type( sanitize_title( strtolower( $singular ) ), $args );
}


/**
 * Individual post type functions | Indicadores
 */
function register_indicadores() {
	$singular = 'Indicador';
	$plural   = 'Indicadores';

	// Set label names for the WP backend UI.
	$labels = array(
		'name'               => _x( $plural, 'Post Type General Name', 'text_domain' ),
		'singular_name'      => _x( $singular, 'Post Type Singular Name', 'text_domain' ),
		'menu_name'          => __( $plural, 'text_domain' ),
		'parent_item_colon'  => __( 'Padre ' . $singular, 'text_domain' ),
		'all_items'          => __( 'Todos los ' . $plural, 'text_domain' ),
		'view_item'          => __( 'Ver ' . $singular, 'text_domain' ),
		'add_new_item'       => __( 'Agregar nuevo ' . $singular, 'text_domain' ),
		'add_new'            => __( 'Agregar nuevo', 'text_domain' ),
		'edit_item'          => __( 'Editar ' . $singular, 'text_domain' ),
		'update_item'        => __( 'Actualizar ' . $singular, 'text_domain' ),
		'search_items'       => __( 'Buscar ' . $singular, 'text_domain' ),
		'not_found'          => __( 'No hemos encontrado resultados', 'text_domain' ),
		'not_found_in_trash' => __( 'No hemos encontrado resultados en la papelera', 'text_domain' ),
	);

	// Set post type options.
	$args = array(
		'label'               => __( strtolower( $plural ), 'text_domain' ),
		'description'         => __( 'My list of ' . strtolower( $plural ), 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array(
			//'editor',
			//'excerpt',
			'revisions',
			'title',
			// phpcs:disable
			//'author',
			//'custom-fields',
			//'comments',
			//'page-attributes',
			//'post-formats',
			'thumbnail',
			// phpcs:enable
		),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		// Icons can be found at: https://developer.wordpress.org/resource/dashicons/.
		'menu_icon'           => 'dashicons-chart-line',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		'show_in_rest'        => true, // Uncomment for Gutenberg.
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'rewrite'             => array(
			'slug'       => sanitize_title( strtolower( $plural ) ),
			'with_front' => true,
		),
	);
	register_post_type( sanitize_title( strtolower( $plural ) ), $args );
}

/**
 * Individual post type functions | Campañas
 */
function register_campanas() {
	$singular = 'Campaña';
	$plural   = 'Campañas';

	// Set label names for the WP backend UI.
	$labels = array(
		'name'               => _x( $plural, 'Post Type General Name', 'text_domain' ),
		'singular_name'      => _x( $singular, 'Post Type Singular Name', 'text_domain' ),
		'menu_name'          => __( $plural, 'text_domain' ),
		'parent_item_colon'  => __( 'Padre ' . $singular, 'text_domain' ),
		'all_items'          => __( 'Todas las ' . $plural, 'text_domain' ),
		'view_item'          => __( 'Ver ' . $singular, 'text_domain' ),
		'add_new_item'       => __( 'Agregar nueva ' . $singular, 'text_domain' ),
		'add_new'            => __( 'Agregar nueva', 'text_domain' ),
		'edit_item'          => __( 'Editar ' . $singular, 'text_domain' ),
		'update_item'        => __( 'Actualizar ' . $singular, 'text_domain' ),
		'search_items'       => __( 'Buscar ' . $singular, 'text_domain' ),
		'not_found'          => __( 'No hemos encontrado resultados', 'text_domain' ),
		'not_found_in_trash' => __( 'No hemos encontrado resultados en la papelera', 'text_domain' ),
	);

	// Set post type options.
	$args = array(
		'label'               => __( strtolower( $plural ), 'text_domain' ),
		'description'         => __( 'My list of ' . strtolower( $plural ), 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array(
			//'editor',
			//'excerpt',
			'revisions',
			'title',
			// phpcs:disable
			//'author',
			//'custom-fields',
			//'comments',
			//'page-attributes',
			//'post-formats',
			'thumbnail',
			// phpcs:enable
		),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		// Icons can be found at: https://developer.wordpress.org/resource/dashicons/.
		'menu_icon'           => 'dashicons-megaphone',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		// 'show_in_rest' =>true, // Uncomment for Gutenberg.
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'rewrite'             => array(
			'slug'       => sanitize_title( strtolower( $singular ) ),
			'with_front' => false,
		),
	);
	register_post_type( sanitize_title( strtolower( $singular ) ), $args );
}


/**
 * Individual post type functions | Staff
 */
function register_staff() {
	$singular = 'Staff';
	$plural   = 'Staff';

	// Set label names for the WP backend UI.
	$labels = array(
		'name'               => _x( $plural, 'Post Type General Name', 'text_domain' ),
		'singular_name'      => _x( $singular, 'Post Type Singular Name', 'text_domain' ),
		'menu_name'          => __( $plural, 'text_domain' ),
		'parent_item_colon'  => __( 'Padre ' . $singular, 'text_domain' ),
		'all_items'          => __( 'Todo el ' . $plural, 'text_domain' ),
		'view_item'          => __( 'Ver ' . $singular, 'text_domain' ),
		'add_new_item'       => __( 'Agregar nuevo ' . $singular, 'text_domain' ),
		'add_new'            => __( 'Agregar nuevo', 'text_domain' ),
		'edit_item'          => __( 'Editar ' . $singular, 'text_domain' ),
		'update_item'        => __( 'Actualizar ' . $singular, 'text_domain' ),
		'search_items'       => __( 'Buscar ' . $singular, 'text_domain' ),
		'not_found'          => __( 'No hemos encontrado resultados', 'text_domain' ),
		'not_found_in_trash' => __( 'No hemos encontrado resultados en la papelera', 'text_domain' ),
	);

	// Set post type options.
	$args = array(
		'label'               => __( strtolower( $plural ), 'text_domain' ),
		'description'         => __( 'My list of ' . strtolower( $plural ), 'text_domain' ),
		'labels'              => $labels,
		'supports'            => array(
			//'editor',
			//'excerpt',
			'revisions',
			'title',
			// phpcs:disable
			//'author',
			//'custom-fields',
			//'comments',
			//'page-attributes',
			//'post-formats',
			//'thumbnail',
			// phpcs:enable
		),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'menu_position'       => 5,
		// Icons can be found at: https://developer.wordpress.org/resource/dashicons/.
		'menu_icon'           => 'dashicons-businessperson',
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => true,
		// 'show_in_rest' =>true, // Uncomment for Gutenberg.
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'capability_type'     => 'post',
		'rewrite'             => array(
			'slug'       => sanitize_title( strtolower( $singular ) ),
			'with_front' => false,
		),
	);
	register_post_type( sanitize_title( strtolower( $singular ) ), $args );
}


// Change dashboard Posts to Noticias
function cp_change_post_object() {
	$get_post_type              = get_post_type_object( 'post' );
	$labels                     = $get_post_type->labels;
	$labels->name               = 'Noticias';
	$labels->singular_name      = 'Noticias';
	$labels->add_new            = 'Agregar Noticias';
	$labels->add_new_item       = 'Agregar Noticias';
	$labels->edit_item          = 'Editar Noticias';
	$labels->new_item           = 'Noticias';
	$labels->view_item          = 'Ver Noticias';
	$labels->search_items       = 'Buscar Noticias';
	$labels->not_found          = 'No hemos encontrado Noticias';
	$labels->not_found_in_trash = 'No hemos encontrado Noticias en la papelera';
	$labels->all_items          = 'Todas las Noticias';
	$labels->menu_name          = 'Noticias';
	$labels->name_admin_bar     = 'Noticias';
}