<?php
/**
 * File of useful functions Axe like to use.
 */


/**
 * Join a list if items with, comma, comma, and put and before the last one.
 * - e.g. John Smith, John Smith and John Smith
 *
 * @param array  $list
 * @param string $conjunction
 *
 * @return mixed|string|null
 */
function natural_language_join( array $list, $conjunction = 'and' ) {

	$last = array_pop( $list );

	if ( $list ) {
		return implode( ', ', $list ) . ' ' . $conjunction . ' ' . $last;
	}

	return $last;
}

/**
 * @param      $url
 * @param bool $internal
 *
 * @return false|string|null
 */
function get_file_size( $url, $internal = true ) {

	if ( $internal ) {
		$path        = wp_parse_url( $url, PHP_URL_PATH );
		$server_path = $_SERVER['DOCUMENT_ROOT'] . $path;

		if ( file_exists( $server_path ) ) {
			return size_format( filesize( $server_path ) );
		}

		return null;
	}

	// phpcs:disable
	$ch = curl_init();
	curl_setopt( $ch, CURLOPT_HEADER, true );
	curl_setopt( $ch, CURLOPT_NOBODY, true );
	curl_setopt( $ch, CURLOPT_FOLLOWLOCATION, true );
	curl_setopt( $ch, CURLOPT_URL, $url );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_exec( $ch );

	$size = curl_getinfo( $ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD );
	// phpcs:enable

	if ( $size > 0 ) {
		return size_format( $size );
	}

	return null;
}


/**
 * Function to generate args to get all content authored / linked to a person.
 *
 * @param $post_id
 *
 * @return array
 */
function get_person_content_args( $post_id ) {
	return array(
		'fields'         => 'ids',
		'post_type'      => 'any',
		'post_status'    => 'publish',
		// phpcs:ignore
		'meta_query'     => array(
			'relation' => 'OR',
			array(
				'compare' => 'LIKE',
				'key'     => 'authors',
				'value'   => '"' . $post_id . '"',
			),
			// phpcs:disable
			// [
			// 	'compare' => 'LIKE',
			// 	'key'     => 'components_%_people',
			// 	'value'   => '"' . $post_id . '"',
			// ],
			// [
			//     'compare' => 'LIKE',
			//     'key'     => 'header_sidebar_contacts',
			//     'value'   => '"' . $post_id . '"',
			// ],
			// [
			//     'compare' => 'LIKE',
			//     'key'     => 'sidebar_content',
			//     'value'   => '"' . $post_id . '"',
			// ],
			// phpcs:enable
		),
		'posts_per_page' => 8,
		// phpcs:ignore
		'meta_key'       => get_option( 'custom_date_field', 'post_date' ),
		'meta_type'      => 'DATE',
		'orderby'        => 'meta_value',
	);
}

/**
 * Modify the SQL before it is executed to convert related content meta_queries into wheres.
 *
 * @param $where
 *
 * @return mixed
 */
function person_where( $where ) {

	global $wpdb;

	$where = str_replace( "meta_key = 'components_%", "meta_key LIKE 'components_%", $wpdb->remove_placeholder_escape( $where ) );

	return $where;
}

//add_filter( 'posts_where', 'person_where' );

/**
 * Get an array of slugs from an ACF taxonomy field.
 *
 * @param string $field_name
 * @param int    $post_id
 *
 * @return array
 */
function get_slugs_from_taxonomy_field( string $field_name, int $post_id ): array {

	$tax_terms_to_show = get_field( $field_name, $post_id );
	$tax_slugs         = array();

	if ( ! empty( $tax_terms_to_show ) ) {
		$tax_slugs = array_map(
			function ( $object ) {
				return $object->slug;
			},
			$tax_terms_to_show
		);
	}

	return $tax_slugs;
}


/**
 * Function to get array of All Authors OR Featured Authors
 *
 * @param      $post_id
 * @param bool $all
 *
 * @return array
 */
function get_authors( $post_id ) {

	$authors         = get_field( 'authors', $post_id );
	$authors_to_show = array();

	if ( ! empty( $authors ) ) {
		foreach ( $authors as $author_id ) {
			$first_name = get_field( 'first_name', $author_id );
			$last_name  = get_field( 'last_name', $author_id );

			if ( empty( $first_name ) && empty( $last_name ) ) {
				$name = get_the_title( $author_id );
			} else {
				$name = $first_name . ' ' . $last_name;
			}

			$authors_to_show[] = array(
				'id'   => $author_id,
				'name' => $name,
			);
		}
	}

	return $authors_to_show;
}

function get_authors_with_links( $post_id ) {

	$authors       = get_field( 'authors', $post_id );
	$authors_links = array();

	if ( ! empty( $authors ) ) {
		foreach ( $authors as $author_id ) {
			$first_name = get_field( 'first_name', $author_id );
			$last_name  = get_field( 'last_name', $author_id );
			$link       = get_permalink( $author_id );

			if ( empty( $first_name ) && empty( $last_name ) ) {
				$name = get_the_title( $author_id );
			} else {
				$name = $first_name . ' ' . $last_name;
			}

			$author_links[] = '<a href="' . $link . '" title="View ' . $name . '\'s profile">' . $name . '</a>';
		}
	}

	return natural_language_join( $author_links );
}

/**
 * Function to return the post type taking into account each of the subtype taxonomies.
 *
 * @param $post_id
 *
 * @return mixed
 */
function get_post_type_label( $post_id = null ) {

	if ( empty( $post_id ) ) {
		$post_id = get_the_ID();
	}

	$post_type     = get_post_type( $post_id );
	$post_type_obj = get_post_type_object( $post_type );

	switch ( $post_type ) {
		// phpcs:disable

		case 'resource':
			$post_type_label = get_first_taxonomy_label( $post_id, 'resource-subtype' );
			break;

		case 'event':
			$post_type_label = get_first_taxonomy_label( $post_id, 'event-subtype' );
			break;

		case 'organisation':
			$post_type_label = get_first_taxonomy_label( $post_id, 'organisation-subtype' );
			break;
		// phpcs:enable

		default:
			if ( ! empty( $post_type_obj ) ) {
				$post_type_label = $post_type_obj->labels->singular_name;
			}
			break;
	}

	if ( empty( $post_type_label ) ) {
		$post_type_label = '';
	}

	return $post_type_label;
}

/**
 * Get a post's taxonomy type - return the first one if more than one
 *
 * @param $post_id
 * @param $taxonomy
 *
 * @return mixed
 */
function get_first_taxonomy_label( $post_id, $taxonomy ) {

	$post_type     = get_post_type( $post_id );
	$post_type_obj = get_post_type_object( $post_type );

	$tax_terms = wp_get_post_terms( $post_id, $taxonomy );

	if ( ! empty( $tax_terms[0] ) ) {
		$post_type_label = $tax_terms[0]->name;
	} else {
		$post_type_label = $post_type_obj->labels->singular_name;
	}

	return $post_type_label;
}

/**
 * Return an array of terms related to a post
 *
 * @param        $post_id
 * @param        $tax
 * @param string $value
 *
 * @return array
 */
function get_post_tax_terms( $post_id, $tax, string $value = 'name' ) {

	$term_objects = wp_get_post_terms( $post_id, $tax );
	$terms        = array();

	if ( ! empty( $term_objects ) ) {
		foreach ( $term_objects as $term ) {
			$terms[] = $term->$value;
		}
	}

	return $terms;
}

/**
 * Array of objects to array of Object-Property strings
 *
 * @param $array
 * @param $property
 *
 * @return array
 */
function object_strings( $array, $property ) {

	$results = [];

	foreach ( $array as $object ) {
		$results[] = $object->$property;
	}

	return $results;
}

/**
 * Function to Obfuscate emails against spam.
 *
 * @param mixed $email The email address you want to obfuscate.
 *
 * @return string the obfuscated email string
 */
function protect_email( $email ): string {

	$new_mail = '';

	if ( ! empty( $email ) ) {
		$p = str_split( trim( $email ) );

		foreach ( $p as $val ) {
			$new_mail .= '&#' . ord( $val ) . ';';
		}
	}

	return $new_mail;
}

/**
 * Get Top Parent Post Id
 */
function get_top_parent_id( $post_id = null ) {
	global $post;

	if ( ! empty( $post_id ) ) {
		$post = get_post( $post_id );
	}

	$parent = $post->ID;

	if ( $post->post_parent ) {
		$ancestors = get_post_ancestors( $post->ID );
		$root      = count( $ancestors ) - 1;
		$parent    = $ancestors[ $root ];
	}

	return $parent;
}

/**
 * @param        $text
 *                  String which is to be shortened.
 * @param int    $length
 *                  Length by which to shorten the string.
 * @param string $ending
 *                  String to show at the end
 * @param bool   $exact
 *                  Whether to cut words in the middle or not
 * @param bool   $consider_html
 *                  Whether or not to close all open HTML tags
 *
 * @return string the substring
 */
function _html_substr( $text, $length = 100, $ending = '...', $exact = false, $consider_html = true ) {

	if ( $consider_html ) {
		// If the plain text is shorter than the maximum length, return the whole text.
		if ( strlen( preg_replace( '/<.*?>/', '', $text ) ) <= $length ) {
			return $text;
		}

		// splits all html-tags to scanable lines.
		preg_match_all( '/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER );
		$total_length = strlen( $ending );
		$open_tags    = array();
		$truncate     = '';
		foreach ( $lines as $line_matchings ) {
			// if there is any html-tag in this line, handle it and add it (uncounted) to the output.
			if ( ! empty( $line_matchings[1] ) ) {
				// if it's an "empty element" with or without xhtml-conform closing slash (f.e. <br/>).
				if ( preg_match( '/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1] ) ) {
					// do nothing
					// if tag is a closing tag (f.e. </b>).
				} elseif ( preg_match( '/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings ) ) {
					// delete tag from $open_tags list.
					$pos = array_search( $tag_matchings[1], $open_tags, true );
					if ( false !== $pos ) {
						unset( $open_tags[ $pos ] );
					}
					// if tag is an opening tag (f.e. <b>).
				} elseif ( preg_match( '/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings ) ) {
					// add tag to the beginning of $open_tags list.
					array_unshift( $open_tags, strtolower( $tag_matchings[1] ) );
				}

				// add html-tag to $truncate'd text.
				$truncate .= $line_matchings[1];
			}

			// calculate the length of the plain text part of the line; handle entities as one character.
			$content_length = strlen( preg_replace( '/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $line_matchings[2] ) );
			if ( $total_length + $content_length > $length ) {
				// the number of characters which are left.
				$left            = $length - $total_length;
				$entities_length = 0;

				// search for html entities.
				if ( preg_match_all( '/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE ) ) {
					// calculate the real length of all entities in the legal range.
					foreach ( $entities[0] as $entity ) {
						if ( $entity[1] + 1 - $entities_length <= $left ) {
							$left --;
							$entities_length += strlen( $entity[0] );
						} else {
							// no more characters left.
							break;
						}
					}
				}

				$truncate .= substr( $line_matchings[2], 0, $left + $entities_length );
				// maximum length is reached, so get off the loop.
				break;
			} else {
				$truncate     .= $line_matchings[2];
				$total_length += $content_length;
			}

			// if the maximum length is reached, get off the loop.
			if ( $total_length >= $length ) {
				break;
			}
		}
	} else {
		if ( strlen( $text ) <= $length ) {
			return $text;
		} else {
			$truncate = substr( $text, 0, $length - strlen( $ending ) );
		}
	}

	// if the words shouldn't be cut in the middle...
	if ( ! $exact ) {
		// ...search the last occurance of a space..
		$spacepos = strrpos( $truncate, ' ' );

		if ( isset( $spacepos ) ) {
			// ...and cut the text in this position
			$truncate = substr( $truncate, 0, $spacepos );
		}
	}

	// add the defined ending to the text.
	$truncate .= $ending;

	if ( $consider_html ) {
		// close all unclosed html-tags.
		foreach ( $open_tags as $tag ) {
			$truncate .= '</' . $tag . '>';
		}
	}

	return $truncate;
}

/**
 * Generate a custom excerpt at the specified length and append with ellipses if necessary.
 *
 * @param string $excerpt
 * @param string $content
 * @param int    $char_length determines the length of the excerpt
 * @param bool   $trim_excerpt determines whether to shorten the excerpt if it is more than $char_length
 *
 * @return string
 *              custom excerpt
 */
function custom_the_excerpt( string $excerpt, string $content, int $char_length = 300, bool $trim_excerpt = false ): string {
	$excerpt = strip_tags( $excerpt, '<br><br/><br />' );
	$content = strip_tags( $content, '<br><br/><br />' );

	$excerpt = html_entity_decode( $excerpt, ENT_QUOTES | ENT_HTML5, 'UTF-8' );
	$content = html_entity_decode( $content, ENT_QUOTES | ENT_HTML5, 'UTF-8' );

	if ( empty( $excerpt ) && ! empty( $content ) && strlen( $content ) > 0 ) {
		if ( strlen( $content ) > $char_length ) {
			$html = _html_substr( $content, $char_length, '...', false, true );
		} else {
			$html = _html_substr( $content, strlen( $content ), '', false, true );
		}
	} elseif ( ! empty( $excerpt ) && $trim_excerpt ) {
		if ( strlen( $excerpt ) > $char_length ) {
			$html = _html_substr( $excerpt, $char_length, '...', false, true );
		} else {
			$html = _html_substr( $excerpt, strlen( $excerpt ), '', false, true );
		}
	} else {
		$html = _html_substr( $excerpt, strlen( $excerpt ), '', false, true );
	}

	return $html;
}

/**
 * Get the slug of the post.
 *
 * @return String the post's slug
 */
function get_slug() {
	global $post;

	$post_data = get_post( $post->ID, ARRAY_A );

	return $post_data['post_name'];
}

/**
 * Echo the post's slug
 */
function the_slug() {

	global $post;
	$post_data = get_post( $post->ID, ARRAY_A );
	$slug      = $post_data['post_name'];

	echo esc_attr( $slug );
}

if ( ! function_exists( 'wp_archive_staff' ) ) {
	function wp_archive_staff( $taxonomy, $ppp ) {
		$args = array(
			'post_type'      => 'staff',
			'posts_per_page' => $ppp,
			'post_status'    => 'publish',
			'no_found_rows'  => true,
			'tax_query'      => array(
				array(
					'taxonomy' => 'roles',
					'field'    => 'slug',
					'terms'    => $taxonomy,
				)
			),
		);

		// The Query
		$query_quotes = new WP_Query( $args );
		$count        = $query_quotes->post_count;
		$tax_label    = get_term_by( 'slug', $taxonomy, 'roles' );
		$tax_name     = $tax_label->name;

		// The Loop
		if ( $query_quotes->have_posts() ) {
			if ( $count >= 1 ) {
				echo '<section class="c-staff c-staff-' . $taxonomy . '">';
				echo '<div class="c-staff__inner o-container">';
				echo '<div class="c-referentes__title">';
				echo $tax_name;
				echo '</div>';
				echo '<div class="c-staff__list">';
				while ( $query_quotes->have_posts() ) : $query_quotes->the_post();
					get_template_part( 'template-parts/components/card', 'staff' );
				endwhile;
				wp_reset_postdata();
				echo '</div></div></section>';
			}
		}
	}
}

if ( ! function_exists( 'wp_archive_informes' ) ) {
	function wp_archive_informes( $ppp ) {
		$args = array(
			'post_type'      => 'informe',
			'posts_per_page' => $ppp,
			'post_status'    => 'publish',
			'no_found_rows'  => true,

		);

		// The Query
		$query_informes = new WP_Query( $args );
		$count          = $query_informes->post_count;

		// The Loop
		if ( $query_informes->have_posts() ) {
			if ( $count > 0 && is_front_page() ) {
				echo '<section id="informes" class="c-informes">
						<div class="c-informes__inner o-container">';
				while ( $query_informes->have_posts() ) : $query_informes->the_post();
					get_template_part( 'template-parts/components/card', 'informes' );
				endwhile;
				wp_reset_postdata();
				echo '</div></section>';
			}
		}
	}
}

function template_chooser( $template ) {
	global $wp_query;
	$post_type = get_query_var( 'post_type' );
	if ( $wp_query->is_search && $post_type == 'informe' ) {
		return locate_template( 'informes.php' );  //  redirect to informes.php
	}

	return $template;
}

add_filter( 'template_include', 'template_chooser' );


function pagination_bar( $custom_query ) {

	$total_pages = $custom_query->max_num_pages;
	$big         = 999999999; // need an unlikely integer

	if ( $total_pages > 1 ) {
		$current_page = max( 1, get_query_var( 'paged' ) );

		echo paginate_links( array(
			'base'      => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format'    => '?paged=%#%',
			'current'   => $current_page,
			'total'     => $total_pages,
			'prev_text' => '<span><</span>',
			'next_text' => '<span>></span>'
		) );
	}
}

add_action( 'init', 'my_remove_editor_from_post_type' );
function my_remove_editor_from_post_type() {
	remove_post_type_support( 'page', 'editor' );
	remove_post_type_support( 'post', 'editor' );
}


//Page Slug Body Class
function add_slug_body_class( $classes ) {
	global $post;
	if ( isset( $post ) ) {
		$classes[] = $post->post_type . '-' . $post->post_name;
	}

	return $classes;
}

add_filter( 'body_class', 'add_slug_body_class' );

// Allow editors to see access the Menus page under Appearance but hide other options
// Note that users who know the correct path to the hidden options can still access them
function hide_menu() {
	$user = wp_get_current_user();

	// Check if the current user is an Editor
	if ( in_array( 'editor', (array) $user->roles ) ) {

		// They're an editor, so grant the edit_theme_options capability if they don't have it
		if ( ! current_user_can( 'edit_theme_options' ) ) {
			$role_object = get_role( 'editor' );
			$role_object->add_cap( 'edit_theme_options' );
		}

		// Hide the Themes page
		remove_submenu_page( 'themes.php', 'themes.php' );

		// Hide the Widgets page
		remove_submenu_page( 'themes.php', 'widgets.php' );

		// Hide the Customize page
		remove_submenu_page( 'themes.php', 'customize.php' );

		// Remove Customize from the Appearance submenu
		global $submenu;
		unset( $submenu['themes.php'][6] );
	}
}

add_action( 'admin_menu', 'hide_menu', 10 );

add_action( 'after_setup_theme', 'remove_header_background', 100 );
function remove_header_background() {
	remove_theme_support( 'custom-background' );
	remove_theme_support( 'custom-header' );
}

