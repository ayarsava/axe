<?php
/* Template Name: Donar */
?>


<?php

if ( isset( $_POST['submit'] ) ) {
	$tuCuotaToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNWE1ODBmM2U4MGM4MzVlNDIxNmE3MTUwYmQ2OTVjNzRjM2I2NDFlNjk0MWNiMWY0MDIxNDQ1ZDFmNTZkNTdjZDdjZGJlN2M3OWE2MDI4M2IiLCJpYXQiOjE2MzcyNTAxNzIuNTEyNTI3LCJuYmYiOjE2MzcyNTAxNzIuNTEyNTM1LCJleHAiOjE2Njg3ODYxNzIuNTA0ODQsInN1YiI6Ijc0Iiwic2NvcGVzIjpbXX0.N6fm7H6yjvPV3KCzlxF_LhF4S8fFHOfNmoag2W3W4IU2fZJq3AnMfmly2PI315OFtpngDy4qYvCsT1U8gdgXsUFS2_hOzykvbdQQW9gO9fDS2VJyfhuE0ETJmhBS7EOWYpxJR2p-7XH1_Ly86lmTZopWSwaXDW88SZFEXlSzR2DcSj3r8MoUj19pkKxMv-6OwN7zMMNX2cSJvo4iXDi9B7fw4ofNRe6hQGaCwpUdn4X7QNVd7dHyUhVjCm8p4vrkHER4za-gzZlXkj2Rp7g2HflbBS5InZO0VrOyT51bC98kABoTjLq8-CZlwOVfuY3-rkykK8gdMSbubLuG5UF3sw-rzbfqTo262V981fqRgjJCLcT97YGBI_7EatSQflwZGdslf4fQw-Bt-FzCak0J08K4tteGS3-HILXWohwOzt3HQQXqzuQayyBFagknoEcpFg8WTHVa6rQqLPt0cpsTwVf2d_CAfbHz5-Tev3Uw6asr-t5sY7jrhdA8F5tg5Dcg6nIDEw3qpTHKZjB4eh8Mwb3RTIYhWaAq028pghy-QhJil0ge_7hZ2IBP4umzH7CriunBmQrRnECMJpbLSRwWDceSjgpGg2KrvEAogonBozkrBhDbZ70N1jWzwAwEwjxi15deBjO2TEjnvQgwuci50Qc8iPhRxuNPizjYlbU977g";
	$endpoint     = 'https://sandbox.tucuota.com/api/sessions';
	$name         = $_POST['nombre'] . ' ' . $_POST['apellido'];
	$amount       = $_POST['amount'];
	$description  = round( $amount / 30 ) . ' pesos por día';
	$post_data    = [
		'kind'           => 'subscription',
		'interval_unit'  => "monthly",
		'success_url'    => 'https://www.google.com/',
		'customer_name'  => $name,
		'customer_email' => $_POST['email'],
		'amount'         => $amount,
		'description'    => $description,

	];
	$headers      = [
		'Authorization: Bearer ' . $tuCuotaToken
	];


	// Prepare data
	$ch = curl_init( $endpoint );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLINFO_HEADER_OUT, true );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_data );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

	// Api call
	$response     = curl_exec( $ch );
	$responseCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

	// close connection
	curl_close( $ch );

	$response = json_decode( $response, true );

	if ( $responseCode >= 400 ) {
		echo "<pre>" . print_r( $response ) . "</pre>";
	} else {
		$url = $response['data']['public_uri'];
		wp_redirect( $url );
		exit;
	}

} else {
	get_header();

	$pid  = get_the_ID();
	$size = 'full'; // (thumbnail, medium, large, full or custom size)

	$subtitulo   = get_field( 'subtitle' );
	$description = get_field( 'description' );

	$banner_titulo = get_field( 'banner_titulo' );
	$banner_texto  = get_field( 'banner_texto' );

	$featured_img_url = get_the_post_thumbnail_url( $pid, 'full' ); // Imagen
	?>
	<div class="c-page">
		<div class="c-datos__featured-image">
			<?php if ( $featured_img_url ) { ?>
				<div class="c-datos__featured-image-inner"
					 style="background-image:url(' <?php echo $featured_img_url; ?>');">
				</div>
				<?php
			} ?>
		</div>
		<header class="c-donar__header">
			<div class="c-donar__header-container o-container o-container-narrow">
				<div class="c-donar__header-inner">
					<?php the_title( '<h1 class="c-donar__header-heading">', '</h1>' ); ?>
					<?php echo '<div class="c-donar__header-subtitle">' . $subtitulo . '</div>' ?>
					<div class="c-banner__interno">
						<?php echo '<h2 class="c-banner__interno-title">' . $banner_titulo . '</h2>' ?>
						<?php echo '<div class="c-banner__interno-texto">' . $banner_texto . '</div>' ?>
					</div>
				</div>
			</div>
		</header>
		<div class="c-shape__05">
			<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120"
				 preserveAspectRatio="none">
				<path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z"
					  class="shape-fill"></path>
			</svg>
		</div>
	</div>
	<div class="c-donar">
		<section class="c-features o-section" style="margin-top:200px">
			<div class="c-features__container o-container o-container-extra-narrow black">
				<div class="c-hero__form-wrapper">
					<div class="c-hero__form-wrapper-title">Formá parte de la comunidad</div>
					<?php get_template_part( 'template-parts/components/donar-form' ); ?>
				</div>
			</div>
		</section>
	</div>
	<?php
}
?>

<?php
get_footer();
