<?php
/* Template Name: Por qué educacion */

get_header();
$pid  = get_the_ID();
$size = 'full'; // (thumbnail, medium, large, full or custom size)

$subtitulo   = get_field( 'subtitle' );
$description = get_field( 'description' );

$banner_titulo = get_field( 'banner_titulo' );
$banner_texto  = get_field( 'banner_texto' );

$featured_img_url = get_the_post_thumbnail_url( $pid, 'full' ); // Imagen

?>
	<div style="position:relative" class="c-page"<?php
	if ( ! empty( $featured_img_url ) ) {
		echo 'style="background:transparent;"';
	} ?>
	>

		<div class="c-datos__featured-image c-pqe">
			<?php if ( $featured_img_url ) { ?>
				<div class="c-datos__featured-image-inner"
					 style="background-image:url(' <?php echo $featured_img_url; ?>');">
				</div>
				<?php
			} ?>
		</div>


		<header class="c-donar__header">
			<div class="c-donar__header-container o-container o-container-narrow">
				<div class="c-donar__header-inner">
					<?php the_title( '<h1 class="c-datos__header-heading">', '</h1>' ); ?>
					<?php echo '<div class="c-donar__header-subtitle">' . $subtitulo . '</div>' ?>
					<div class="c-banner__interno">
						<?php echo '<h2 class="c-banner__interno-title">' . $banner_titulo . '</h2>' ?>
						<div class="c-banner__interno-title-onda"></div>
						<?php echo '<div class="c-banner__interno-texto">' . $banner_texto . '</div>' ?>
					</div>
				</div>
			</div>
		</header>
		<div class="c-shape__05">
			<svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120"
				 preserveAspectRatio="none">
				<path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z"
					  class="shape-fill"></path>
			</svg>
		</div>
	</div>
	<div class="por-que-educacion">


	<section class="c-features o-section">
		<div class="c-features__container o-container  o-container-narrow black">
			<?php
			get_template_part( 'template-parts/components/features' );
			?>
		</div>
	</section>

<?php
$banner_secundario = get_field( 'banner_secundario' );
if ( $banner_secundario ) {
	get_template_part( 'template-parts/components/banner_secundario' );
}
?>

<?php
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
		echo '<section class="c-text-block o-section">
		<div class="c-text-block__container o-container o-container-extra-narrow">
			<div class="c-text-block__content">';
		echo $description;
		echo '</div>
		</div>
	</section>';
	}
}
?>


<?php
if ( have_rows( 'quotes_repeater' ) ):
	echo '<section id="quotes" class="c-quotes">
		<div class="c-quotes__inner o-container">
			<div class="c-quotes-main-title">
			La educación como prioridad: Una demanda de todos los argentinos
			</div>
			<div class="container">
				<div class="c-quotes__slick slick single-item">';
	while ( have_rows( 'quotes_repeater' ) ) : the_row();
		get_template_part( 'template-parts/components/slide', 'quotes' );
	endwhile;

	echo '</div>
	</div>
	</div>
	<div class="c-shape__03">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1400 163.4">
			<path d="M.5,0S49,63,162.5,21.3s172.6,1,208.5,14.6,151.3-9.7,161.9-18.4c13.6-5.9,13.6,14.5,26.2,23.2,0,0,117.4,94.1,232.8,90.2s184.3-51.4,277.4-51.4,135.8,22.3,192,15.5S1400,42.7,1400,42.7l-.5,120.7H0Z"
				  style="fill:#fff"/>
		</svg>
	</div>
	</section>';
endif;
?>


<?php
$cta_titulo      = get_field( 'cta_titulo', $pid );
$cta_descripcion = get_field( 'cta_descripcion', $pid );
if ( $cta_titulo || $cta_descripcion ) {
	?>
	<section class="c-text-block o-section cta-pqe">
		<div class="c-text-block__container o-container o-container-extra-narrow">
			<?php
			get_template_part( 'template-parts/components/call-to-action' );
			?>
		</div>
	</section>
	</div>
	<?php
}
?>

	</div>
<?php
get_footer();