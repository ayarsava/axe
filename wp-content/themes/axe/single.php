<?php
/**
 * Single post template.
 */

get_header();

// Start the Loop.
while ( have_posts() ) {
	the_post();

	global $post_id;
	?>

	<!-- Main content -->
	<section class="c-article">
		<article class="c-article__inner">

			<?php get_template_part( 'template-parts/single-parts/single-header' ); ?>

			<?php
			if ( is_singular( 'indicadores' ) ) {
				get_template_part( 'template-parts/components/embed' );
			} else {
				get_template_part( 'template-parts/single-parts/single-content' );
			}
			?>

			<?php
			if ( is_singular( 'campana' ) ) {
				?>
				<div class="c-informes__search-results o-container">
					<div class="c-informes__inner">
						<?php
						get_template_part( 'template-parts/single-parts/single-related' );
						?>
					</div>
				</div>
			<?php } ?>
		</article>
	</section>
	<?php
}
get_footer();
