<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AxE
 */

?>
</main>
<footer class="c-footer">
	<div class="c-footer__inner o-container">
		<?php if ( get_field( 'newsletter_footer_title', 'option' ) ) { ?>
			<div class="c-footer__heading">
				<?php the_field( 'newsletter_footer_title', 'option' ); ?>
			</div><!-- .site-info -->
		<?php } ?>
		<div class="c-footer__newsletter">
			<div class="c-footer__form-wrapper c-footer__form">
				<?php get_template_part( 'template-parts/components/newsletter' ); ?>
			</div>
		</div>
		<div class="c-footer__credits">
			<?php the_field( 'credits_text', 'option' ); ?>
			<div class="c-footer__social">
				<?php if ( get_field( 'twitter_url', 'option' ) ) { ?>
					<a href="https://www.twitter.com/<?php the_field( 'twitter_url', 'option' ); ?>"
					   target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/i-twitter.svg"
											alt="" class="c-footer__social-icon"/></a>
				<?php } ?>
				<?php if ( get_field( 'facebook_url', 'option' ) ) { ?>
					<a href="<?php the_field( 'facebook_url', 'option' ); ?>"
					   target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/i-facebook.svg"
											alt=""
											class="c-footer__social-icon"/></a>
				<?php } ?>
				<?php if ( get_field( 'instagram_url', 'option' ) ) { ?>
					<a href="<?php the_field( 'instagram_url', 'option' ); ?>"
					   target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/i-instagram.svg"
											alt=""
											class="c-footer__social-icon"/></a>
				<?php } ?>
				<?php if ( get_field( 'whatsapp_number', 'option' ) ) { ?>
					<a href="https://api.whatsapp.com/send?phone=549<?php the_field( 'whatsapp_number', 'option' ); ?>"
					   target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/i-whatsapp.svg"
											alt=""
											class="c-footer__social-icon"/></a>
				<?php } ?>
				<?php if ( get_field( 'gitlab_url', 'option' ) ) { ?>
					<a href="<?php the_field( 'gitlab_url', 'option' ); ?>"
					   target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/assets/i-gitlab.svg"
											alt=""
											class="c-footer__social-icon"/></a>
				<?php } ?>


			</div>
		</div>
	</div>
</footer><!-- .c-footer -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
