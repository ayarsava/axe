<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package AxE
 */

get_header();
?>
	<header class="entry-header o-container o-container-narrow">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<main id="primary" class="site-main que-hacemos">
		<section class="c-features o-section">
			<div class="c-features__container o-container o-container-narrow">
				<?php
				get_template_part( 'template-parts/components/features' );
				?>
			</div>
		</section>
		<div class="line"></div>
	</main><!-- #main -->

<?php
get_footer();
