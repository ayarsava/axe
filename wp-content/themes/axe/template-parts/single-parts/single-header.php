<?php
/**
 * Template part for displaying heading in single
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package AxE
 */

/*
$sub_title          = get_field( 'subtitle', $pid );
$standfirst         = get_field( 'standfirst', $pid );
$description        = get_field( 'description', $pid );
$single_type        = get_post_type_label( $pid );
$date               = get_the_date( 'd M Y', $pid );
$file_download      = get_field( 'file_download', $pid );
$external_link_url  = get_field( 'external_link_url', $pid );
$external_link_text = get_field( 'external_link_text', $pid );

$indicador_descripcion = get_field( 'indicador_descripcion', $pid );
*/


$pid = get_the_ID();

// Common fields
$featured_img_url = get_the_post_thumbnail_url( $pid, 'full' ); // Imagen

// Noticias fields
$noticias_volanta   = get_field( 'noticias_volanta', $pid ); // Text.
$noticias_subtitulo = get_field( 'noticias_subtitulo', $pid ); // Text.
$noticias_acciones  = get_field( '', $pid ); // Taxonomy.

// Informes fields
$subtitle                   = get_field( 'subtitle', $pid ); // Text.
$published_date             = get_field( 'published_date', $pid ); // Date picker.
$documento_adjunto          = get_field( 'documento_adjunto', $pid ); // Clone bajo el nombre documento_adjunto.
$file_url                   = wp_get_attachment_url( $documento_adjunto );
$documento_adjunto_etiqueta = get_field( 'documento_adjunto_etiqueta', $pid );


// Indicadores fields
$indicador_embed       = get_field( 'indicador_embed', $pid ); // Text.
$indicador_nivel       = get_field( 'indicador_nivel', $pid ); // Taxonomy.
$indicador_area        = get_field( 'indicador_area', $pid ); // Taxonomy.
$indicador_provincia   = get_field( 'indicador_provincia', $pid ); // Taxonomy.
$indicador_descripcion = get_field( 'indicador_descripcion', $pid ); // Wysiwyg Editor.


// Campañas fields
$subtitulo = get_field( 'subtitulo ', $pid ); // Clone bajo el nombre campana_general. Text.
//$campana_accion       = get_field( 'campana_accion', $pid ); // Taxonomy.
$campana_numero       = get_field( 'campana_numero', $pid ); // Text.
$campana_etiqueta     = get_field( 'campana_etiqueta', $pid ); // Text.
$campana_related_news = get_field( 'campana_related_news', $pid ); // Relationship


?>
<header>
	<div class="c-article__header-content-wrapper o-container">
		<div class="c-article__metadata">
			<?php
			// Noticias
			// Volanta, Titulo, Bajada
			if ( is_singular( 'post' ) ) {
				if ( ! empty( $noticias_volanta ) ) { ?>
					<span class="c-article__volanta"><?php echo esc_html( $noticias_volanta ); ?></span>
				<?php } ?>
				<h1 class="c-article__heading"><?php the_title(); ?></h1>
			<?php }

			// Informes
			// Fecha, Titulo, Bajada
			elseif ( is_singular( 'informe' ) ) {
				if ( ! empty( $published_date ) ) { ?>
					<span class="c-article__volanta"><?php echo esc_html( $published_date ); ?></span>
				<?php } ?>
				<h1 class="c-article__heading"><?php the_title(); ?></h1>

			<?php }

			// Indicadores
			// (Indicadores o Datos), Titulo, Bajada
			elseif ( is_singular( 'indicadores' ) ) {

				if ( $indicador_nivel ) {
					echo '<span class="c-article__volanta screen-reader-text">Indicadores - Nivel ';
					foreach ( $indicador_nivel as $nivel ) {
						$nivel = get_term( $nivel );
						echo $nivel->name;
					}
					echo '</span>';
				} else { ?>
					<div class="c-article__volanta">
						<span style="text-decoration: underline"><a href="/datos" style="color:#222;">Datos</a></span>
					</div>
				<?php } ?>
				<h1 class="c-article__heading"><?php the_title(); ?></h1>

				<?php if ( ! empty( $indicador_descripcion ) ) {
					echo '<div class="c-article__descripcion screen-reader-text">';
					echo wp_kses( $indicador_descripcion, 'post' );
					echo '</div>';
				}
			}
			// Campañas
			// (Taxonomia Accion), Titulo, Bajada
			elseif ( is_singular( 'campana' ) ) {
				$campana_accion = get_field( 'campana_accion' );


				$term_id = get_field( 'campana_accion', $pid );
				if ( $term_id ) {
					$term_name = get_term( $term_id )->name;
					$term_link = get_term_link( $term_id );
					echo '<div class="c-article__volanta">';
					echo '<span style="text-decoration: underline"><a href="' . $term_link . '">' . $term_name . '</a></span>';
					echo '</div>';
				}
				?>
				<h1 class="c-article__heading"><?php the_title(); ?></h1>

			<?php } ?>

			<?php if ( ! empty( $noticias_subtitulo ) ) { ?>
				<div class="c-article__bajada"><?php echo esc_html( $noticias_subtitulo ); ?></div>
			<?php } ?>
			<?php if ( ! empty( $subtitle ) ) { ?>
				<div class="c-article__bajada"><?php echo esc_html( $subtitle ); ?></div>
			<?php } ?>
			<?php if ( ! empty( $subtitulo ) ) { ?>
				<div class="c-article__bajada"><?php echo esc_html( $subtitulo ); ?></div>
			<?php } ?>

			<?php
			if ( ! empty( $documento_adjunto ) ) {
				?>
				<div class="c-informe__cta" style="margin-top: 30px; margin-bottom:30px;"><a
							href="<?php echo esc_html( $file_url ); ?>"
							class="o-button"
							target="_blank">
						<?php
						if ( ! empty( $documento_adjunto_etiqueta ) ) {
							echo $documento_adjunto_etiqueta;
						} else {
							echo 'Descargar el informe completo';
						}
						?>
					</a></div>
			<?php } ?>

		</div>


		<?php if ( is_singular( 'post' ) ) {
			if ( $featured_img_url ) { ?>
				<div class="c-article__featured-image">
					<img src="<?php echo $featured_img_url; ?>" class="c-article__featured-image-inner">
				</div>
			<?php }
		} else { ?>
			<div class="c-article__featured-image">
				<?php if ( $featured_img_url ) { ?>
					<div class="c-article__featured-image-inner"
						 style="background-image:url(' <?php echo $featured_img_url; ?>');"></div>
				<?php } ?>
			</div>
			<?php
		} ?>
		<?php if ( ! empty( $campana_numero ) ) {
			echo '<div class="destaque"><div class="destaque-inner"><span class="numero">' . $campana_numero . '</span><span class="etiqueta">' . $campana_etiqueta . '</span></div></div>';
		} ?>
	</div>
	<?php if ( is_singular( 'campana' ) ) { ?>
		<div class="c-shape__datos">
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1404 110.4">
				<path d="M0,110.3S118.5,74.1,268.3,79,455.9,29.7,620.5,13.2,767,46.1,859.2,46.1s130-42.8,265-46.1S1404,82.3,1404,82.3v28Z"
					  transform="translate(0 0.1)" style="fill:#fff"/>
			</svg>
		</div>
	<?php } ?>
</header>