<?php
/**
 * Template part for displaying related content in single
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package AxE
 */

?>

<?php
$featured_posts = get_field( 'campana_related_news' );
if ( $featured_posts ): ?>
	<?php foreach ( $featured_posts as $post ):

		// Setup this post for WP functions (variable must be named $post).
		setup_postdata( $post ); ?>
		<?php echo get_template_part( 'template-parts/components/card-noticias' ); ?>
	<?php endforeach; ?>
	<?php
	// Reset the global post object so that the rest of the page works correctly.
	wp_reset_postdata(); ?>
<?php endif; ?>