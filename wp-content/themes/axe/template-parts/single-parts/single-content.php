<?php
/**
 * Template part for displaying content in single
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package AxE
 */

$pid = get_the_ID();

// Common fields
$featured_img_url = get_the_post_thumbnail_url( $pid, 'full' ); // Imagen

// Noticias
$noticias_contenido = get_field( 'noticias_contenido', $pid ); // Wysiwyg Editor.

// Informes
$description = get_field( 'description', $pid ); // Wysiwyg Editor.

// Campañas
$declaracion_url          = get_field( 'declaracion_url', $pid ); // Url
$declaracion_url_etiqueta = get_field( 'declaracion_url_text', $pid ); // Url

?>

<div class="c-article__content o-container">

	<div class="c-article__content-inner<?php if ( ! empty( $declaracion_url ) ) {
		echo ' has-declaracion';
	} ?>">


		<?php
		if ( ! empty( $noticias_contenido ) ) {
			echo wp_kses( $noticias_contenido, 'post' );
		} elseif ( ! empty( $description ) ) {
			echo wp_kses( $description, 'post' );
		}
		?>

	</div>
	<?php
	if ( ! empty( $declaracion_url ) ) {
		?>
		<div class="c-declaracion"><a href="<?php echo esc_html( $declaracion_url ); ?>"
									  class="o-button o-button-yellow"
									  target="_blank">
				<?php
				if ( ! empty( $declaracion_url_etiqueta ) ) {
					echo $declaracion_url_etiqueta;
				} else {
					echo 'Lee la declaración completa';
				}
				?>
			</a></div>
	<?php } ?>
</div>