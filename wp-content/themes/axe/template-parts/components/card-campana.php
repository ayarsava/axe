<?php
$pid   = get_the_ID();
$image = wp_get_attachment_image_src( get_post_thumbnail_id( $pid ), 'single-post-thumbnail' );

// Load field value and convert to numeric timestamp.
$unixtimestamp = strtotime( get_field( 'published_date' ) );


$title    = get_the_title();
$subtitle = get_field( 'subtitle', $pid ); // Text.
?>
<div class="c-campana__card">
	<?php if ( $image ) {
		?>
		<div class="c-campana__card-image-wrapper">
			<div class="c-campana__card-image">
				<img src="<?php echo $image[0]; ?>">
			</div>
		</div>
		<?php
	} ?>
	<div class="c-campana__card-content">
		<?php the_title( '<h3 class="c-campana__card-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
		<div class="c-campana__card-description"><?php echo esc_html( $subtitle ); ?></div>
	</div>
</div>