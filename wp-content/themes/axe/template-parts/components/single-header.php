<?php
/**
 * Single header - called at single.php
 */
?>
<?php
$pid                = get_the_ID();
$sub_title          = get_field( 'subtitle', $pid );
$standfirst         = get_field( 'standfirst', $pid );
$description        = get_field( 'description', $pid );
$single_type        = get_post_type_label( $pid );
$date               = get_the_date( 'd M Y', $pid );
$file_download      = get_field( 'file_download', $pid );
$external_link_url  = get_field( 'external_link_url', $pid );
$external_link_text = get_field( 'external_link_text', $pid );

$indicador_descripcion = get_field( 'indicador_descripcion', $pid );

$featured_img_url = get_the_post_thumbnail_url( $post->ID, 'full' );

?>
<header>
	<div class="c-article__header-content-wrapper o-container">
		<div class="c-article__metadata">
			<span class="c-article__volanta"><?php echo esc_html( $single_type ); ?></span>
			<h1 class="c-article__heading"><?php the_title(); ?></h1>
			<?php if ( ! empty( $sub_title ) ) { ?>
				<h2 class="c-article__bajada"><?php echo esc_html( $sub_title ); ?></h2>
			<?php } ?>
			<?php if ( ! empty( $description ) ) { ?>
				<div><?php echo wp_kses( $description, 'post' ); ?></div>
			<?php } ?>

			<?php if ( ! empty( $indicador_descripcion ) ) { ?>
				<div><?php echo wp_kses( $indicador_descripcion, 'post' ); ?></div>
			<?php } ?>
		</div>
		<?php if ( $featured_img_url ) { ?>
			<div class="c-article__featured-image">
				<div class="c-article__featured-image-inner"
					 style="background-image:url('<?php echo $featured_img_url; ?>');">

				</div>
			</div>

			<?php
		}
		?>

		<div>
			<?php if ( ! empty( $external_link['external_link_url'] ) ) { ?>
				<a href="<?php esc_url( $external_link['external_link_url'] ); ?>">
					<?php if ( ! empty( $external_link['external_link_text'] ) ) { ?>
						<?php echo esc_html( $external_link['external_link_text'] ); ?>
					<?php } else { ?>
						<?php echo 'Read ' . esc_html( $single_type ); ?>
					<?php } ?>
				</a>
			<?php } ?>
			<?php
			if ( ! empty( $file_download['file'] ) ) {
				?>
				<a href="<?php esc_url( $file_download['file'] ); ?>">
					<?php if ( ! empty( $file_download['button_text'] ) ) { ?>
						<?php echo esc_html( $file_download['button_text'] ); ?>
					<?php } else { ?>
						Download
					<?php } ?>
				</a>
				<?php
			}
			?>

		</div>
	</div>
</header>
