<?php
$pid           = get_the_ID();
$embed_portada = get_field( 'indicador_embed_portada', $pid );
$embed_datos   = get_field( 'indicador_embed', $pid );
?>


<?php
$featured_posts = get_field( 'contenido_relacionado' );
if ( $featured_posts ): ?>
	<section class="c-informes">
		<div class="c-informes__inner o-container">
			<?php foreach ( $featured_posts as $post ):

				// Setup this post for WP functions (variable must be named $post).
				setup_postdata( $post ); ?>
				<?php get_template_part( 'template-parts/components/card', 'informes' ); ?>
			<?php endforeach; ?>
			<?php
			// Reset the global post object so that the rest of the page works correctly.
			wp_reset_postdata(); ?>
		</div>
	</section>
<?php endif; ?>