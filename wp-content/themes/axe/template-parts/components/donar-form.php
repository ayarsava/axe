<form method="POST" class="c-donar__form c-donar__form">
	<div class="c-donar__form-nombre">
		<label for="nombre">Nombre</label>
		<input type="text" id="nombre" name="nombre" required>
	</div>
	<div class="c-donar__form-apellido">
		<label for="apellido">Apellido</label>
		<input type="text" id="apellido" name="apellido" required>
	</div>
	<div class="c-donar__form-email">
		<label for="email">Email</label>
		<input id="email" name="email" type="email" required>
	</div>
	<fieldset class="c-donar__form-amount">
		<?php

		// Check rows exists.
		if ( have_rows( 'donar_call-to-actions' ) ):
			echo '<fieldset class="c-donar__form-amount">';
			// Loop through rows.
			while ( have_rows( 'donar_call-to-actions' ) ) : the_row();

				// Load sub field value.
				$donar_etiqueta    = get_sub_field( 'donar_etiqueta' );
				$donar_descripcion = get_sub_field( 'donar_descripcion' );
				$donar_prefix      = get_sub_field( 'donar_prefix' );
				$donar_suffix      = get_sub_field( 'donar_suffix' );
				echo '<input type="radio" id="amount-' . get_row_index() . '" name="amount" value="';
				if ( is_numeric( $donar_etiqueta ) ) {
					echo $donar_etiqueta;
				} else {
					echo '1';
				}
				echo '"';

				if ( get_row_index() == 1 ) {
					echo 'checked';
				};
				echo '/>';
				echo '<label for="amount-' . get_row_index() . '">';
				if ( $donar_prefix ) {
					echo '<span class="c-donar__form-amount-prefix">' . $donar_prefix . '</span>';
				}
				echo '<span class="c-donar__form-amount-number">' . $donar_etiqueta . '</span>';
				if ( $donar_suffix ) {
					echo '<span class="c-donar__form-amount-suffix">' . $donar_suffix . '</span>';
				}
				echo '<span class="c-donar__form-amount-descripcion">' . $donar_descripcion . '</span></label>';
			endwhile;
			echo '</fieldset>';
		endif;
		?>
	</fieldset>
	<div class="c-donar__form-button">
		<input type="submit" name="submit" id="submit" value="Donar"
			   class="c-donar__form-field o-button__block">
	</div>
</form>