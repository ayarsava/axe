<?php

$pid           = get_the_ID();
$cta_boton     = get_field( 'cta_boton', $pid );
$cta_boton_url = get_field( 'cta_boton_url', $pid );

$cta_titulo      = get_field( 'cta_titulo', $pid );
$cta_descripcion = get_field( 'cta_descripcion', $pid );
?>

<div class="c-call-to-action">
	<div class="c-call-to-action__heading"><?php echo esc_html( $cta_titulo ); ?></div>
	<div class="c-call-to-action__content">
		<?php echo $cta_descripcion;
		if ( $cta_boton_url ) {
			?>

			<div class="c-banner__boton">
				<a href="<?php echo esc_html( $cta_boton_url ); ?>"
				   title="<?php echo esc_html( $cta_boton ); ?>"
				   class="o-button o-container"><?php echo esc_html( $cta_boton ); ?></a>
			</div>
			<?php
		}
		?>
	</div>
</div>