<?php
$embed_form = the_field( 'newsletter_form_embed', 'option' );
?>

<div class="c-footer__form-wrapper">
	<?php echo esc_html( $embed_form ); ?>
</div>
