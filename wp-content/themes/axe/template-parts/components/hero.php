<?php

// Get the Video Fields
$video_mp4    = get_field( 'mp4_video' ); // MP4 Field Name
$video_webm   = get_field( 'webm_video' ); // WEBM Field Name
$video_poster = get_field( 'poster_image' ); // Poster Image Field Name

// Build the  Shortcode
$attr = array(
	'mp4'      => $video_mp4,
	'webm'     => $video_webm,
	'poster'   => $video_poster,
	'preload'  => 'auto',
	'autoplay' => true,
	'controls' => false,
	'loop'     => true,
);


?>

<section class="c-hero">
	<?php if ( ! empty( $video_mp4 ) ) { ?>
		<div class="flex-video">
			<video class="fullscreen" src="<?php echo $video_mp4; ?>" playsinline autoplay muted loop>
			</video>
		</div>
	<?php }
	if ( ! empty( $video_poster ) ) { ?>
		<div class="c-hero__image" style="background-image: url('<?php echo $video_poster; ?>"></div>
	<?php } ?>
	<div class="c-hero__wrapper">
		<div class="o-container c-hero__inner">
			<div class="c-hero__heading">
				<h1 class="c-hero__heading-title"><?php the_field( 'titulo_de_banner' ); ?></h1>
				<div class="c-hero__heading-description"><?php the_field( 'descripcion_de_banner' ); ?>
				</div>
			</div>

			<div class="c-hero__form-wrapper">
				<div class="c-hero__form-wrapper-title">Formá parte de la comunidad</div>
				<?php get_template_part( 'template-parts/components/donar-form' ); ?>

			</div>
		</div>
	</div>
	<?php if ( is_front_page() ) {
		echo '<div class="c-shape__05"><svg data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1200 120" preserveAspectRatio="none">
				<path d="M321.39,56.44c58-10.79,114.16-30.13,172-41.86,82.39-16.72,168.19-17.73,250.45-.39C823.78,31,906.67,72,985.66,92.83c70.05,18.48,146.53,26.09,214.34,3V0H0V27.35A600.21,600.21,0,0,0,321.39,56.44Z" class="shape-fill"></path>
			</svg></div>';
	}
	?>
</section>