<?php
$titulo_mapa = get_field( 'titulo_mapa' );
?>
<?php if ( $titulo_mapa ) {
	echo '<div class="c-referentes__title">' . $titulo_mapa . '</div>';
} ?>
<div class="c-mapa__embed-container">
	<?php the_field( 'mapa' ); ?>
</div>