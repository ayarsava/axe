<?php
$pid           = get_the_ID();
$quote_name    = get_sub_field( 'quote_name' );
$quote_cargo   = get_sub_field( 'quote_cargo' );
$quote_espacio = get_sub_field( 'quote_espacio' );
$quote_imagen  = get_sub_field( 'quote_imagen' );
$quote_texto   = get_sub_field( 'quote_texto' );
$size          = 'full'; // (thumbnail, medium, large, full or custom size)
$imgArr        = wp_get_attachment_image_src( $quote_imagen, $size );
?>
<div class="c-quotes__item-wrapper">
	<div class="c-quotes__item">
		<div class="c-quotes__item-content">

			<?php
			if ( ! empty( $quote_texto ) ) {
				echo '<div class="c-quotes__item-content">';
				echo $quote_texto;
				echo '</div>';
			}
			?>

			<div class="c-quotes__item-metadata">
				<?php
				if ( ! empty( $quote_name ) ) {
					echo $quote_name;
				}
				if ( ! empty( $quote_cargo ) ) {
					echo ', ' . $quote_cargo;
				}
				if ( ! empty( $quote_espacio ) ) {
					echo ', ' . $quote_espacio;
				}
				?>
			</div>
		</div>
		<div class="c-quotes__item-image-wrapper">
			<?php
			if ( $imgArr[0] ) {
				echo '<div class="c-quotes__item-image" style="background-image: url(';
				echo $imgArr[0];
				echo ')"></div>';
			}
			?>
		</div>
	</div>
</div>