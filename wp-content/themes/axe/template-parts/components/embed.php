<?php
$pid                  = get_the_ID();
$embed_portada        = get_field( 'indicador_embed_portada', $pid );
$embed_portada_mobile = get_field( 'indicador_embed_portada_mobile', $pid );
$embed_datos          = get_field( 'indicador_embed', $pid );
$embed_datos_mobile   = get_field( 'indicador_embed_mobile', $pid );
$es_provincia         = get_field( 'es_provincia', $pid );
?>

<section class="c-embed">
	<div class="c-embed__inner">
		<div class="c-embed__embed-container">
			<?php
			if ( ! empty( $embed_datos ) ) {
				if ( $es_provincia == 1 ) { ?>
					<iframe class="c-embed__iframe c-embed__iframe-desktop" src="<?php echo $embed_datos; ?>"
							width="100%"
							allowfullscreen="allowfullscreen" border="0" id="embed-datos"
							height="800">
					</iframe>
				<?php } else { ?>
					<iframe class="c-embed__iframe c-embed__iframe-desktop" src="<?php echo $embed_datos; ?>"
							width="100%"
							allowfullscreen="allowfullscreen" border="0" id="embed-datos"
							height="1280">
					</iframe>
				<?php }
			} ?>

			<?php
			if ( ! empty( $embed_datos_mobile ) ) {
				if ( $es_provincia == 1 ) { ?>
					<iframe class="c-embed__iframe-mobile" src="<?php echo $embed_datos_mobile; ?>" width="100%"
							allowfullscreen="allowfullscreen" border="0" id="embed-datos"
							height="800">
					</iframe>
				<?php } else { ?>
					<iframe class="c-embed__iframe-mobile" src="<?php echo $embed_datos_mobile; ?>" width="100%"
							allowfullscreen="allowfullscreen" border="0" id="embed-datos"
							height="1280">
					</iframe>
				<?php }
			} ?>

			<?php
			if ( $es_provincia == 1 ) {
				if ( ! empty( $embed_portada ) ) {
					?>
					<iframe class="c-embed__iframe" src="<?php echo $embed_portada; ?>" width="100%"
							allowfullscreen="allowfullscreen" border="0" height="1800"></iframe>
				<?php }
				if ( ! empty( $embed_portada_mobile ) ) {
					?>
					<iframe class="c-embed__iframe-mobile" src="<?php echo $embed_portada_mobile; ?>" width="100%"
							allowfullscreen="allowfullscreen" border="0" height="1800"></iframe>
				<?php }
			} ?>
		</div>
	</div>
</section>

