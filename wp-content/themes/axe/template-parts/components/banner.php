<?php
$pid                = get_the_ID();
$size               = 'full'; // (thumbnail, medium, large, full or custom size)
$banner_titulo      = get_field( 'banner_titulo', $pid );
$banner_descripcion = get_field( 'banner_descripcion', $pid );
$banner_boton       = get_field( 'banner_boton_texto', $pid );
$banner_url         = get_field( 'banner_boton_url', $pid );
$banner_imagen_id   = get_field( 'banner_imagen' );
$banner_imagen_arr  = wp_get_attachment_image_src( $banner_imagen_id, $size );
$banner_tipo        = get_field( 'banner_tipo' );
?>

<?php
if ( $banner_url || $banner_titulo || $banner_descripcion ) {
	if ( $banner_tipo == 1 ) {
		?>
		<div class="c-banner">
			<div class="c-banner__image"
				 style="background-image: url(<?php echo $banner_imagen_arr[0]; ?> );"></div>
			<div class="c-banner__container o-container">
				<div class="c-banner__title"><?php echo esc_html( $banner_titulo ); ?></div>
				<div class="c-banner__content"><?php echo esc_html( strip_tags( $banner_descripcion ) ); ?>
				</div>
				<div class="c-banner__boton">
					<a href="<?php echo esc_html( $banner_url ); ?>"
					   title="<?php echo esc_html( $banner_boton ); ?>"
					   class="o-button o-container"><?php echo esc_html( $banner_boton ); ?></a>
				</div>
			</div>
		</div>

		<?php
	} else {
		?>

		<section class="c-statement">
			<div class="c-statement__header-content-wrapper o-container">
				<div class="c-statement__metadata">
					<h2 class="c-statement__heading"><?php echo esc_html( $banner_titulo ); ?></h2>
					<div class="c-statement__bajada"><?php echo esc_html( strip_tags( $banner_descripcion ) ); ?>
					</div>
				</div>
				<div class="c-statement__featured-image">
					<img src="<?php echo $banner_imagen_arr[0]; ?>" class="c-article__featured-image-inner">
				</div>
			</div>
		</section>

		<?php
	}
}