<?php
$pid    = get_the_ID();
$image  = get_field( 'staff_imagen' );
$size   = 'full'; // (thumbnail, medium, large, full or custom size)
$title  = get_the_title();
$resena = get_field( 'staff_resena', $pid ); // Text.
?>
<div class="c-staff__card">

	<div class="c-staff__card-image-wrapper">
		<div class="c-staff__card-image">
			<?php
			if ( $image ) {
				echo wp_get_attachment_image( $image, $size );
			}
			?>
		</div>
	</div>

	<div class="c-staff__card-content">
		<?php the_title( '<h3 class="c-staff__card-title">', '</h3>' ); ?>
		<div class="c-staff__card-description"><?php echo esc_html( $resena ); ?></div>
	</div>
</div>