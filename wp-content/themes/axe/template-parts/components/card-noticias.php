<?php
$pid      = get_the_ID();
$src      = wp_get_attachment_image_src( get_post_thumbnail_id( $pid ), array( 5600, 1000 ), false, '' );
$title    = get_the_title();
$date     = get_field( 'published_date' );
$subtitle = get_field( 'subtitle', $pid ); // Text.
?>
<div class="c-informes__card">
	<?php if ( $src ) { ?>
		<div class="c-informes__card-image-wrapper">
			<div class="c-informes__card-image">
				<?php the_post_thumbnail( 'medium', array( 'class' => 'img-fluid' ) ); ?>
			</div>
		</div>
	<?php } ?>

	<div class="c-informes__card-content">
		<div class="c-informes__card-date"><?php echo $date; ?></div>
		<?php the_title( '<h3 class="c-informes__card-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
		<div class="c-informes__card-description"><?php echo esc_html( $subtitle ); ?></div>
		<a href="<?php echo get_permalink( $pid ) ?>" class="c-informes__card-link">Ver más</a>
	</div>
</div>