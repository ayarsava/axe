<?php
$pid      = get_the_ID();
$image    = wp_get_attachment_image_src( get_post_thumbnail_id( $pid ), 'single-post-thumbnail' );
$date     = get_field( 'published_date' );
$title    = get_the_title();
$subtitle = get_field( 'subtitle', $pid ); // Text.
?>
<div class="c-acciones__card destacada  o-container">

	<div class="c-acciones__card-content">
		<div class="c-acciones__card-date"><?php echo $date; ?></div>

		<?php the_title( '<h3 class="c-acciones__card-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h3>' ); ?>
		<div class="c-acciones__card-description"><?php echo esc_html( $subtitle ); ?></div>
		<a href="<?php echo get_permalink( $pid ) ?>" class="c-acciones__card-link o-button">Ver más</a>
	</div>
	<div class="c-acciones__card-image-wrapper">
		<div class="c-acciones__card-image">
			<img src="<?php echo $image[0]; ?>">
		</div>
	</div>
</div>