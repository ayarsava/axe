<?php
$features_volanta = get_field( 'features_volanta' );
$features_titulo  = get_field( 'features_titulo' );
$features_bajada  = get_field( 'features_bajada' );
$columns          = get_field( 'columns' );
$features_boton   = get_field( 'features_boton' );

?>

<?php if ( $features_volanta || $features_titulo || $features_bajada ): ?>
	<div class="c-features__heading">

		<?php
		if ( $features_volanta ):
			echo '<div class="c-features__heading-volanta">' . $features_volanta . '</div>';
		endif;
		if ( $features_titulo ):
			echo '<h1 class="c-features__heading-title">' . $features_titulo . '</h1>';
		endif;
		if ( $features_bajada ):
			echo '<div class="c-features__heading-description">' . $features_bajada . '</div>';
		endif;
		?>

	</div>
<?php endif; ?>
	<div class="c-features__content">
<?php

// Check rows exists.
if ( have_rows( 'feature_item' ) ):

	while ( have_rows( 'feature_item' ) ) :
		the_row();
		$feature_titulo      = get_sub_field( 'feature_titulo' );
		$feature_bajada      = get_sub_field( 'feature_bajada' );
		$feature_descripcion = get_sub_field( 'feature_descripcion' );
		$feature_imagen      = get_sub_field( 'feature_imagen' );
		$feature_link        = get_sub_field( 'feature_link' );


		echo '<div class="c-features__content-item c-features__content-item-columns-' . esc_html( $columns ) . '">';
		echo '<div class="c-features__content-item-wrapper">';

		if ( $feature_imagen ):
			if ( $feature_link ):
			$feature_link_url    = $feature_link['url'];
			$feature_link_title  = $feature_link['title'];
			$feature_link_target = $feature_link['target'] ? $feature_link['target'] : '_self';
				echo '<a href="' . $feature_link_url . '">';
			endif;
			echo '<div class="c-features__content-item-image">' . wp_get_attachment_image( $feature_imagen, 'full' ) .
			     '</div>';
			if ( $feature_link ):
				echo '</a>';
			endif;
		endif;
		if ( $feature_titulo ):
			echo '<div class="c-features__content-item-heading">' . esc_html( $feature_titulo ) . '</div>';
		endif;
		if ( $feature_bajada ):
			echo '<div class="c-features__content-item-bajada">' . esc_html( $feature_bajada ) . '</div>';
		endif;
		if ( $feature_descripcion ):
			echo '<div class="c-features__content-item-descripcion">' . esc_html( $feature_descripcion ) . '</div>';
		endif;
		if ($columns == 1):
			if ( $feature_link ):
				$feature_link_url    = $feature_link['url'];
				$feature_link_title  = $feature_link['title'];
				$feature_link_target = $feature_link['target'] ? $feature_link['target'] : '_self';
				echo '<div class="c-features__content-item-btn"><a href="' . esc_url( $feature_link_url ) . '" target="' . esc_attr( $feature_link_target ) . '" class="c-informes__card-link">';
				if ($feature_link_title):
					echo esc_html( $feature_link_title );
				else :
					echo 'Ver más';
				endif;
				echo '</a></div>';
			endif;
			endif;
		echo '</div></div>';
		endwhile;

		endif;

		$features_boton            = get_field( 'features_boton' );
		if ( $features_boton ):
		$link_url = $features_boton['url'];
		$link_title  = $features_boton['title'];
		$link_target = $features_boton['target'] ? $features_boton['target'] : '_self';
		?>
		<div class="o-container" style="text-align:center;margin-top:30px;">
		<a class="c-features__heading-button" href="<?php echo esc_url( $link_url ); ?>"
																			   target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
		</div>
		<?php endif; ?>