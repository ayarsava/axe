<?php
/**
 * AxE functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package AxE
 */

if ( ! defined( 'axe_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( 'axe_VERSION', '1.0.0' );
}

if ( ! function_exists( 'axe_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function axe_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on AxE, use a find and replace
		 * to change 'axe' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'axe', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'axe' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'axe_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'axe_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function axe_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'axe_content_width', 640 );
}

add_action( 'after_setup_theme', 'axe_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function axe_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'axe' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'axe' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}

add_action( 'widgets_init', 'axe_widgets_init' );

/**
 * Enqueue scripts and styles.
 */

function axe_scripts() {
	wp_enqueue_style( 'google-fonts-mulish', 'https://fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,400;0,600;0,700;1,300;1,400;1,600;1,700', false );
	wp_enqueue_style( 'google-fonts-noticia', 'https://fonts.googleapis.com/css2?family=Noticia+Text:ital,wght@0,400;0,700;1,400;1,700&display=swap', false );
	wp_enqueue_style( 'axe-style', get_stylesheet_uri(), array(), axe_VERSION );
	// wp_style_add_data( 'axe-style', 'rtl', 'replace' );

	wp_enqueue_script( 'axe-navigation', get_template_directory_uri() . '/js/navigation.js', array(), axe_VERSION, true );
	wp_enqueue_script( 'axe-slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array(), axe_VERSION, true );
	wp_enqueue_script( 'axe-custom', get_template_directory_uri() . '/js/custom.js', array( 'jquery' ), axe_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}

add_action( 'wp_enqueue_scripts', 'axe_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_template_directory() . '/inc/woocommerce.php';
}


/**
 * Make sure the ACF Pro plugin is active (the theme requires it).
 */
if ( function_exists( 'is_plugin_active' ) ) {
	$is_acf_active = is_plugin_active( 'advanced-custom-fields-pro/acf.php' );

	if ( ! $is_acf_active ) {
		activate_plugin( 'advanced-custom-fields-pro/acf.php' );
	}
}

/**
 * Enable the ACF options page
 */
if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( [
		'page_title' => 'Options',
		'slug'       => 'custom-options',
		'capability' => 'manage_options',
		'capability' => 'edit_posts',
	] );

	acf_add_options_sub_page( [
		'title'      => 'Contact Settings',
		'slug'       => 'contact-settings',
		'parent'     => 'custom-options',
		'capability' => 'manage_options',
		'capability' => 'edit_posts',
	] );

	acf_add_options_sub_page( [
		'title'      => 'Content Settings',
		'slug'       => 'content-settings',
		'parent'     => 'custom-options',
		'capability' => 'manage_options',
		'capability' => 'edit_posts',
	] );
}


// Custom taxonomies
require_once get_template_directory() . '/inc/functions-taxonomies.php';

// Custom post types.
require_once get_template_directory() . '/inc/functions-post-types.php';

// Soapbox common functions.
require_once get_template_directory() . '/inc/functions-axe.php';


add_image_size( 'square', 800, 800, array() ); // Hard crop left top
