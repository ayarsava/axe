<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AxE
 */


$image = get_field( 'imagen_de_fondo' );

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'axe' ); ?></a>

	<header id="header" class="c-header<?php if ( is_front_page() ) :
		echo ' c-header__homepage'; endif;
	if ( is_user_logged_in() ) {
		echo ' with-admin';
	}
	if ( ! empty( $image ) ) : echo ' c-header__has-background'; endif; ?> js-header">
		<div class="c-header__inner o-container">

			<?php
			the_custom_logo();
			if ( is_front_page() || is_singular( 'campana' ) ) :
				?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="c-header__logo">
					<span class="c-header__logo-image"/></span>
				</a>

			<?php
			else :
				?>
				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" class="c-header__logo">
					<span class="c-header__logo-image"></span>
				</a>
			<?php endif; ?>

			<div id="hamburger" class="c-header__burger js-burger-button hamburglar">

				<div class="burger-icon">
					<div class="burger-container">
						<span class="burger-bun-top"></span>
						<span class="burger-filling"></span>
						<span class="burger-bun-bot"></span>
					</div>
				</div>

				<!-- svg ring containter -->
				<div class="burger-ring">
					<svg class="svg-ring">
						<path class="path" fill="none" stroke="#fff" stroke-miterlimit="10" stroke-width="4"
							  d="M 34 2 C 16.3 2 2 16.3 2 34 s 14.3 32 32 32 s 32 -14.3 32 -32 S 51.7 2 34 2"/>
					</svg>
				</div>
				<!-- the masked path that animates the fill to the ring -->

				<svg width="0" height="0">
					<mask id="mask">
						<path xmlns="http://www.w3.org/2000/svg" fill="none" stroke="#ff0000" stroke-miterlimit="10"
							  stroke-width="4" d="M 34 2 c 11.6 0 21.8 6.2 27.4 15.5 c 2.9 4.8 5 16.5 -9.4 16.5 h -4"/>
					</mask>
				</svg>
				<div class="path-burger">
					<div class="animate-path">
						<div class="path-rotation"></div>
					</div>
				</div>

			</div>


			<div class="c-header__menu-wrapper js-burger-content">
				<nav class="c-main-menu js-main-menu">
					<?php
					wp_nav_menu(
						array(
							'theme_location'  => 'menu-1',
							'menu_id'         => 'primary-menu',
							'container'       => 'ul',
							'container_class' => 'c-main-menu__container',
							'menu_class'      => 'c-main-menu__list',
						)
					);
					?>
				</nav>
			</div>
			<a href="<?php echo esc_url( home_url( '/' ) ); ?>/donar" class="c-header__donar-button o-button">
				Donar
			</a>
		</div>
	</header><!-- .c-header -->


	<main id="content">

