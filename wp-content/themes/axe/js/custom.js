/**
 * File custom.js.
 *
 * Handles custom functions
 */

(function ($) {
    //$('.single-item').slick();

    $('.slick.single-item').slick({
        dots: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
    });


    $(window).scroll(function () {
        if ($(this).scrollTop() > 113) {
            $('#header').addClass("sticky_header");
        } else {
            $('#header').removeClass("sticky_header");
        }
    });


    // Tabs
    // Show the first tab and hide the rest
    $('#tabs-nav li:first-child').addClass('active');
    $('.tab-content').hide();
    $('.tab-content:first').show();

    // Click function
    $('#tabs-nav li').click(function () {
        $('#tabs-nav li').removeClass('active');
        $(this).addClass('active');
        $('.tab-content').hide();

        var activeTab = $(this).find('a').attr('href');
        $(activeTab).fadeIn();
        return false;
    });


    $('document').ready(function () {
        var trigger = $('#hamburger'),
            isClosed = false;
        var burgerContent = $('.js-burger-content');

        trigger.click(function () {
            burgerTime();
        });

        function burgerTime() {
            if (isClosed == true) {
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
                burgerContent.removeClass('is-open');
                burgerContent.addClass('is-closed');
            } else {
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
                burgerContent.addClass('is-open');
                burgerContent.removeClass('is-closed');
            }
        }

    });
})(jQuery);
