<?php
/**
 * The template for displaying archive for Taxonomy Acción
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package AxE
 */

get_header();
?>

	<main id="primary" class="site-main">
		<?php

		$term_id         = get_queried_object()->term_id;
		$args_destacada  = array(
			'post_type'      => 'campana',
			'posts_per_page' => 1,
			'order'          => 'DESC',
			'tax_query'      => array(
				array(
					'taxonomy' => 'acciones',
					'field'    => 'term_id',
					'terms'    => array( $term_id ),  //put more term ids if required
				),
			),
		);
		$query_destacada = new WP_Query( $args_destacada );

		$args  = array(
			'post_type'      => 'campana',
			'posts_per_page' => 3,
			'offset'         => 1,
			'order'          => 'DESC',
			'tax_query'      => array(
				array(
					'taxonomy' => 'acciones',
					'field'    => 'term_id',
					'terms'    => array( $term_id ),  //put more term ids if required
				),
			),
		);
		$query = new WP_Query( $args );

		echo '<section id="acciones" class="c-acciones">';
		echo '<header class="page-header">';
		echo '<div class="heading-inner o-container">';
		echo '<h1 class="page-title">';
		echo single_term_title();
		echo '</h1>';
		the_archive_description( '<div class="archive-description">', '</div>' );
		echo '</div>';
		echo '</header>';

		if ( $query_destacada->have_posts() ) {
			echo '<div class="c-acciones__inner destacada">';
			while ( $query_destacada->have_posts() ) {
				$query_destacada->the_post();
				get_template_part( 'template-parts/components/card-campana-destacada' );
			}
			echo '</div>';

			// Restore original Post Data once finished, IMPORTANT
			wp_reset_postdata();
		}

		if ( $query->have_posts() ) {
			echo '<div class="c-acciones__inner o-container">';
			while ( $query->have_posts() ) {
				$query->the_post();
				get_template_part( 'template-parts/components/card-campana' );
			}
			echo '</div>';


			// Restore original Post Data once finished, IMPORTANT
			wp_reset_postdata();
		}


		$args_adicionales  = array(
			'post_type'      => 'campana',
			'posts_per_page' => 100,
			'offset'         => 4,
			'order'          => 'DESC',
			'tax_query'      => array(
				array(
					'taxonomy' => 'acciones',
					'field'    => 'term_id',
					'terms'    => array( $term_id ),  //put more term ids if required
				),
			),
		);
		$query_adicionales = new WP_Query( $args_adicionales );
		if ( $query_adicionales->have_posts() ) {
			echo '<ul class="c-acciones__inner c-acciones__inner-list o-container">';
			while ( $query_adicionales->have_posts() ) {
				echo '<li>';
				$query_adicionales->the_post();
				the_title(
					sprintf( '<h4 class="card-title h4"><a href="%s" rel="bookmark">', esc_attr( esc_url( get_permalink() ) ) ),
					'</a></h4>'
				);
				echo '</li>';
			}
			echo '</div>';
			// Restore original Post Data once finished, IMPORTANT
			wp_reset_postdata();
		}

		echo '</section>';

		?>


	</main><!-- #main -->

<?php
get_footer();
