<?php
/* Template Name: Donar */

get_header();

$pid              = get_the_ID();
$featured_img_url = get_the_post_thumbnail_url( $pid, 'full' ); // Imagen

$subtitulo   = get_field( 'subtitle' );
$description = get_field( 'description' );
?>

	<style>
        /* Style the form */
        #regForm {
        }

        /* Style the input fields */
        input {
        }

        /* Mark input boxes that gets an error on validation: */
        input.invalid {
        }

        /* Hide all steps by default: */
        .tab {
            display: none;
        }

        /* Make circles that indicate the steps of the form: */
        .step {
            height: 15px;
            width: 15px;
            margin: 0 2px;
            background-color: #bbbbbb;
            border: none;
            border-radius: 50%;
            display: inline-block;
            opacity: 0.5;
        }

        /* Mark the active step: */
        .step.active {
            opacity: 1;
        }

        /* Mark the steps that are finished and valid: */
        .step.finish {
            background-color: #04AA6D;
        }
	</style>
	hola
	<div class="c-donar"<?php
	if ( ! empty( $featured_img_url ) ) {
		echo 'style="background:transparent;"';
	} ?>
	>
		<?php if ( $featured_img_url ) { ?>
			<div class="c-datos__featured-image">
				<div class="c-datos__featured-image-inner"
					 style="background-image:url(' <?php echo $featured_img_url; ?>');">
				</div>
			</div>
			<?php
		} ?>

		<header class="c-donar__header">
			<div class="c-donar__header-container o-container o-container-narrow">
				<div class="c-donar__header-inner">
					<?php the_title( '<h1 class="c-donar__header-heading">', '</h1>' ); ?>
					<?php echo '<div class="c-donar__header-subtitle">' . $subtitulo . '</div>' ?>
				</div>
			</div>
		</header>

		<div class="c-donar__content o-container o-container-narrow">
			<div class="c-donar__form-wrapper">

				<form id="regForm" action="" class="c-donar__form">


					<!-- One "tab" for each step in the form: -->
					<div class="tab">
						<h3>Datos personales:</h3>
						<input type="text" id="customer_id" name="customer_id" oninput="this.className = ''">
						<div class="c-donar__form-nombre">
							<label for="customer_nombre">Nombre</label>
							<input type="text" id="customer_nombre" name="customer_nombre" placeholder="Tu nombre"
								   oninput="this.className = ''">
						</div>
						<div class="c-donar__form-apellido">
							<label for="customer_apellido">Apellido</label>
							<input type="text" id="customer_apellido" name="customer_apellido" placeholder="Tu apellido"
								   oninput="this.className = ''">
						</div>
					</div>

					<div class="tab">
						<h3>Tipo de suscripcion:</h3>

						<div class="c-donar__form-email">
							<label for="customer_email">Email</label>
							<input type="email" id="customer_email" name="customer_email" placeholder="Tu email"
								   oninput="this.className = ''">
						</div>
					</div>

					<div style="overflow:auto;">
						<div style="float:right;">
							<button type="button" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
							<button type="button" id="nextBtn" onclick="nextPrev(1)">Next</button>
						</div>
					</div>

					<!-- Circles which indicates the steps of the form: -->
					<div style="text-align:center;margin-top:40px;">
						<span class="step"></span>
						<span class="step"></span>
					</div>

				</form>


			</div>
		</div>
	</div>

	<script>
        var currentTab = 0; // Current tab is set to be the first tab (0)
        showTab(currentTab); // Display the current tab

        function showTab(n) {
            // This function will display the specified tab of the form ...
            var x = document.getElementsByClassName("tab");
            x[n].style.display = "block";
            // ... and fix the Previous/Next buttons:
            if (n == 0) {
                document.getElementById("prevBtn").style.display = "none";
            } else {
                document.getElementById("prevBtn").style.display = "inline";
            }
            if (n == (x.length - 1)) {
                document.getElementById("nextBtn").innerHTML = "Submit";
            } else {
                document.getElementById("nextBtn").innerHTML = "Next";
            }
            // ... and run a function that displays the correct step indicator:
            fixStepIndicator(n)
        }

        function nextPrev(n) {
            // This function will figure out which tab to display
            var x = document.getElementsByClassName("tab");
            // Exit the function if any field in the current tab is invalid:
            if (n == 1 && !validateForm()) return false;
            // Hide the current tab:
            x[currentTab].style.display = "none";
            // Increase or decrease the current tab by 1:
            currentTab = currentTab + n;
            // if you have reached the end of the form... :
            if (currentTab >= x.length) {
                //...the form gets submitted:
                document.getElementById("regForm").submit();
                return false;
            }
            // Otherwise, display the correct tab:
            showTab(currentTab);
        }

        function validateForm() {
            // This function deals with validation of the form fields
            var x, y, i, valid = true;
            x = document.getElementsByClassName("tab");
            y = x[currentTab].getElementsByTagName("input");
            // A loop that checks every input field in the current tab:
            for (i = 0; i < y.length; i++) {
                // If a field is empty...
                if (y[i].value == "") {
                    // add an "invalid" class to the field:
                    y[i].className += " invalid";
                    // and set the current valid status to false:
                    valid = false;
                }
            }
            // If the valid status is true, mark the step as finished and valid:
            if (valid) {
                document.getElementsByClassName("step")[currentTab].className += " finish";
            }
            return valid; // return the valid status
        }

        function fixStepIndicator(n) {
            // This function removes the "active" class of all steps...
            var i, x = document.getElementsByClassName("step");
            for (i = 0; i < x.length; i++) {
                x[i].className = x[i].className.replace(" active", "");
            }
            //... and adds the "active" class to the current step:
            x[n].className += " active";
        }
	</script>
<?php
get_footer();
