<?php
/* Template Name: Custom Search */

get_header();
?>

	<div class="c-informes entry-header">
		<div class="o-container">
			<div class="c-informes__search">
				<h1 class="c-informes__search-heading">Informes</h1>
				<div class="c-informes__search-subtitle">Formá parte de la comunidad que le da voz a las familias</div>
				<form role="search" action="<?php echo site_url( '/' ); ?>" method="get" id="searchform"
					  class="c-informes__search-form">
					<input class="c-informes__search-input" type="text" name="s" placeholder="Buscar informes"/>
					<input type="hidden" name="post_type" value="informe"/>
					<input class="c-informes__search-button" type="submit" alt="Search" value="Buscar"/>
				</form>
			</div>
		</div>
	</div>

	<section id="informes">
		<div class="c-informes__search-results o-container">
			<div class="c-informes__wrapper">

				<?php
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				$args  = array(
					'post_type'      => 'informe',
					's'              => $s,
					'posts_per_page' => 9,
					'paged'          => $paged
				);
				query_posts( $args );

				if ( have_posts() ) :
					echo '<div class="c-informes__inner">';
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/components/card', 'informes' );
					endwhile;
					echo '</div>';
					echo '<nav class="c-informes__pagination pagination">';
					pagination_bar( $wp_query );
					echo '</nav>';
				else :
					echo 'No hemos encontrado resultados para la búsqueda. Intente nuevamente utilizando otro término';
				endif;
				wp_reset_postdata();
				?>
			</div>
		</div>
	</section>


<?php
get_footer();
