<?php
/* Template Name: Donar */
?>


<?php

if ( isset( $_POST['submit'] ) ) {
	$tuCuotaToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNWE1ODBmM2U4MGM4MzVlNDIxNmE3MTUwYmQ2OTVjNzRjM2I2NDFlNjk0MWNiMWY0MDIxNDQ1ZDFmNTZkNTdjZDdjZGJlN2M3OWE2MDI4M2IiLCJpYXQiOjE2MzcyNTAxNzIuNTEyNTI3LCJuYmYiOjE2MzcyNTAxNzIuNTEyNTM1LCJleHAiOjE2Njg3ODYxNzIuNTA0ODQsInN1YiI6Ijc0Iiwic2NvcGVzIjpbXX0.N6fm7H6yjvPV3KCzlxF_LhF4S8fFHOfNmoag2W3W4IU2fZJq3AnMfmly2PI315OFtpngDy4qYvCsT1U8gdgXsUFS2_hOzykvbdQQW9gO9fDS2VJyfhuE0ETJmhBS7EOWYpxJR2p-7XH1_Ly86lmTZopWSwaXDW88SZFEXlSzR2DcSj3r8MoUj19pkKxMv-6OwN7zMMNX2cSJvo4iXDi9B7fw4ofNRe6hQGaCwpUdn4X7QNVd7dHyUhVjCm8p4vrkHER4za-gzZlXkj2Rp7g2HflbBS5InZO0VrOyT51bC98kABoTjLq8-CZlwOVfuY3-rkykK8gdMSbubLuG5UF3sw-rzbfqTo262V981fqRgjJCLcT97YGBI_7EatSQflwZGdslf4fQw-Bt-FzCak0J08K4tteGS3-HILXWohwOzt3HQQXqzuQayyBFagknoEcpFg8WTHVa6rQqLPt0cpsTwVf2d_CAfbHz5-Tev3Uw6asr-t5sY7jrhdA8F5tg5Dcg6nIDEw3qpTHKZjB4eh8Mwb3RTIYhWaAq028pghy-QhJil0ge_7hZ2IBP4umzH7CriunBmQrRnECMJpbLSRwWDceSjgpGg2KrvEAogonBozkrBhDbZ70N1jWzwAwEwjxi15deBjO2TEjnvQgwuci50Qc8iPhRxuNPizjYlbU977g";
	$endpoint     = 'https://sandbox.tucuota.com/api/sessions';
	$name         = $_POST['nombre'];
	$description  = 'Gracias ' . $name . ' por tu donación';
	$post_data    = [
		'kind'           => 'payment',
		'success_url'    => 'https://www.google.com/',
		'customer_name'  => $_POST['nombre'],
		'customer_email' => $_POST['email'],
		'amount'         => '420',
		'description'    => $description,

	];
	$headers      = [
		'Authorization: Bearer ' . $tuCuotaToken
	];


	// Prepare data
	$ch = curl_init( $endpoint );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLINFO_HEADER_OUT, true );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_data );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

	// Api call
	$response     = curl_exec( $ch );
	$responseCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

	// close connection
	curl_close( $ch );

	$response = json_decode( $response, true );

	if ( $responseCode >= 400 ) {
		echo "<pre>" . print_r( $response ) . "</pre>";
	} else {
		$url = $response['data']['public_uri'];
		wp_redirect( $url );
		exit;
	}

} else {
	get_header();
	?>
	<form method="POST">
		<label for="nombre">Nombre</label>
		<input type="text" id="nombre" name="nombre" required>
		<label for="email">Email</label>
		<input type="text" id="email" name="email" required>
		<input type="submit" name="submit" id="submit" value="Enviar">
	</form>
	<?php
}
?>

<?php
get_footer();
