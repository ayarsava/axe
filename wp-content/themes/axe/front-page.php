<?php
/**
 * Template Name: Home Template
 */

if ( isset( $_POST['submit'] ) ) {
	$tuCuotaToken = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNWE1ODBmM2U4MGM4MzVlNDIxNmE3MTUwYmQ2OTVjNzRjM2I2NDFlNjk0MWNiMWY0MDIxNDQ1ZDFmNTZkNTdjZDdjZGJlN2M3OWE2MDI4M2IiLCJpYXQiOjE2MzcyNTAxNzIuNTEyNTI3LCJuYmYiOjE2MzcyNTAxNzIuNTEyNTM1LCJleHAiOjE2Njg3ODYxNzIuNTA0ODQsInN1YiI6Ijc0Iiwic2NvcGVzIjpbXX0.N6fm7H6yjvPV3KCzlxF_LhF4S8fFHOfNmoag2W3W4IU2fZJq3AnMfmly2PI315OFtpngDy4qYvCsT1U8gdgXsUFS2_hOzykvbdQQW9gO9fDS2VJyfhuE0ETJmhBS7EOWYpxJR2p-7XH1_Ly86lmTZopWSwaXDW88SZFEXlSzR2DcSj3r8MoUj19pkKxMv-6OwN7zMMNX2cSJvo4iXDi9B7fw4ofNRe6hQGaCwpUdn4X7QNVd7dHyUhVjCm8p4vrkHER4za-gzZlXkj2Rp7g2HflbBS5InZO0VrOyT51bC98kABoTjLq8-CZlwOVfuY3-rkykK8gdMSbubLuG5UF3sw-rzbfqTo262V981fqRgjJCLcT97YGBI_7EatSQflwZGdslf4fQw-Bt-FzCak0J08K4tteGS3-HILXWohwOzt3HQQXqzuQayyBFagknoEcpFg8WTHVa6rQqLPt0cpsTwVf2d_CAfbHz5-Tev3Uw6asr-t5sY7jrhdA8F5tg5Dcg6nIDEw3qpTHKZjB4eh8Mwb3RTIYhWaAq028pghy-QhJil0ge_7hZ2IBP4umzH7CriunBmQrRnECMJpbLSRwWDceSjgpGg2KrvEAogonBozkrBhDbZ70N1jWzwAwEwjxi15deBjO2TEjnvQgwuci50Qc8iPhRxuNPizjYlbU977g";
	$endpoint     = 'https://sandbox.tucuota.com/api/sessions';
	$name         = $_POST['nombre'] . ' ' . $_POST['apellido'];
	if ( is_numeric( $_POST['amount'] ) ) {
		$amount = $_POST['amount'];
	} else {
		$amount = '1';
	}
	if ( $amount > 1 ) {
		$editable_amount = false;
	} else {
		$editable_amount = true;
	}
	if ( $amount > 1 ) {
		$description = round( $amount / 30 ) . ' pesos por día';
	} else {
		$description = 'Gracias ' . $name . ' por tu aporte.';
	}

	$post_data = array(
		'kind'           => 'subscription',
		'interval_unit'  => "monthly",
		'success_url'    => 'https://argentinosporlaeducacion.org',
		'customer_name'  => $name,
		'customer_email' => $_POST['email'],
		'amount'         => $amount,
		'description'    => $description,
	);

	if ( $amount < 2 ) {
		$post_data['editable_amount'] = true;
	}


	$headers = [
		'Authorization: Bearer ' . $tuCuotaToken
	];


	// Prepare data
	$ch = curl_init( $endpoint );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch, CURLINFO_HEADER_OUT, true );
	curl_setopt( $ch, CURLOPT_POST, true );
	curl_setopt( $ch, CURLOPT_POSTFIELDS, $post_data );
	curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );

	// Api call
	$response     = curl_exec( $ch );
	$responseCode = curl_getinfo( $ch, CURLINFO_HTTP_CODE );

	// close connection
	curl_close( $ch );

	$response = json_decode( $response, true );

	if ( $responseCode >= 400 ) {
		echo "<pre>" . print_r( $response ) . "</pre>";
	} else {
		$url = $response['data']['public_uri'];
		wp_redirect( $url );
		exit;
	}

} else {
	get_header();

	get_template_part( 'template-parts/components/hero' );
	?>
	<section id="features" class="c-features">
		<div class="o-container c-features__inner">
			<?php
			get_template_part( 'template-parts/components/features' );
			?>
		</div>
	</section>

	<?php
	get_template_part( 'template-parts/components/related-content' );
	?>

	<?php if ( get_field( 'video_oembed' ) ) { ?>

		<section class="c-video o-section">
			<div class="o-container c-video__inner o-container-narrow">
				<?php
				get_template_part( 'template-parts/components/video' );
				?>
			</div>
		</section>
	<?php } ?>


	<?php
	if ( have_rows( 'quotes_repeater' ) ):
		echo '<section id="quotes" class="c-quotes">
		<div class="c-quotes__inner o-container">
			<div class="container">
				<div class="c-quotes__heading">Comunidad El Aula</div>
				<div class="c-quotes__slick slick single-item">';
		while ( have_rows( 'quotes_repeater' ) ) : the_row();
			get_template_part( 'template-parts/components/slide', 'quotes' );
		endwhile;

		echo '</div>
	</div>
	<div class="c-quotes__cta-wraper">
		<div class="c-quotes__cta-wraper__btn o-button">Sumate</div>
	</div>
	</div>
	<div class="c-shape__03">
		<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1400 163.4">
			<path d="M.5,0S49,63,162.5,21.3s172.6,1,208.5,14.6,151.3-9.7,161.9-18.4c13.6-5.9,13.6,14.5,26.2,23.2,0,0,117.4,94.1,232.8,90.2s184.3-51.4,277.4-51.4,135.8,22.3,192,15.5S1400,42.7,1400,42.7l-.5,120.7H0Z"
				  style="fill:#fff"/>
		</svg>
	</div>
	</section>';
	endif;
	?>

	<?php

	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			?>
			<?php the_content(); ?>
			<?php
		}
	}

}

get_footer();
