<?php
/* Template Name: Archivo de Datos */

$pid              = get_the_ID();
$featured_img_url = get_the_post_thumbnail_url( $pid, 'full' ); // Imagen
$subtitulo        = get_field( 'subtitle' );
$description      = get_field( 'description' );

get_header();
?>
	<section class="c-datos"
		<?php
		if ( ! empty( $featured_img_url ) ) {
			echo 'style="background:transparent;"';
		} ?>
	>


		<header class="c-datos__header">
			<div class="c-datos__header-container o-container">
				<div class="c-datos__header-inner">
					<?php the_title( '<h1 class="c-datos__header-heading">', '</h1>' ); ?>
					<?php echo '<div class="c-datos__header-subtitle">' . $subtitulo . '</div>' ?>
				</div>
			</div>
			<?php if ( $featured_img_url ) { ?>
				<div class="c-datos__featured-image">
					<div class="c-datos__featured-image-inner"
						 style="background-image:url(' <?php echo $featured_img_url; ?>');">
					</div>
				</div>
				<?php
			} ?>
			<div class="c-shape__datos">
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1404 110.4">
					<path d="M0,110.3S118.5,74.1,268.3,79,455.9,29.7,620.5,13.2,767,46.1,859.2,46.1s130-42.8,265-46.1S1404,82.3,1404,82.3v28Z"
						  transform="translate(0 0.1)" style="fill:#fff"/>
				</svg>
			</div>
		</header>


		<?php
		if ( ! empty( $description ) ) {

			echo '<div class="c-text-block o-section"><div class="c-text-block__container o-container o-container-extra-narrow"><div class="c-text-block__content">';
			echo $description;
			echo '</div></div></div>';
		}
		?>

		<div class="" id="datos">
			<div class="c-datos__content">
				<div class="c-datos__content-container">
					<div class="c-datos__content-inner o-container">
						<h2 class="c-datos__content-heading">Indicadores</h2>
						<div class="c-datos__content-subtitle">Visualizá y descargá datos de más de 20 indicadores de
							los últimos 20 años
						</div>
					</div>

					<div>
						<?php

						$posts = get_posts( array(
							'post_type'  => 'indicadores',
							'order'      => 'ASC',
							'meta_query' => array(
								array(
									'key'   => 'es_provincia',
									'value' => '0',
								),
							)
						) );

						if ( $posts ) {
							echo '<ul style="width: 100%; text-align: center;padding-left:16px;">';
							foreach ( $posts as $post ) {
								$link = get_the_permalink( $post->ID );
								echo '<li style="display: inline-block;margin-right:15px;margin-bottom:15px;"><a href="' . $link . '" class="o-button">' . get_the_title() . '</a></li>';
							}
							echo '</ul>';
						} else {
							echo 'No hay datos que mostrar';
						}

						?>
						<div class="line"></div>
					</div>

					<div>
						<?php

						$provposts = get_posts( array(
							'post_type'      => 'indicadores',
							'meta_query'     => array(
								array(
									'key'   => 'es_provincia',
									'value' => '1',
								)
							),
							'posts_per_page' => - 1,
						) );

						if ( $provposts ) {
							echo '<div class="c-datos__content-inner o-container">
						<h2 class="c-datos__content-heading" style="margin-top:30px;">Fichas por provincia</h2>
						<div class="c-datos__content-subtitle">Transformar la educación argentina con evidencia,
							consensos y colaboración social
						</div>
					</div>

					<div class="c-datos__content-prov-list o-container"><ul>';
							foreach ( $provposts as $provpost ) {
								$link = get_the_permalink( $provpost->ID );
								echo '<li><a href="' . $link . '">' . get_the_title( $provpost->ID ) . '</a></li>';
							}
							echo '</ul></div>';
						} else {
							echo 'No hay datos que mostrar';
						}

						?>
					</div>

					<?php
					$pid           = get_the_ID();
					$cta_boton     = get_field( 'cta_boton', $pid );
					$cta_boton_url = get_field( 'cta_boton_url', $pid );

					$cta_titulo      = get_field( 'cta_titulo', $pid );
					$cta_descripcion = get_field( 'cta_descripcion', $pid );
					if ( $cta_titulo || $cta_descripcion || $cta_boton ) {
					?>
					<section class="c-text-block o-section">
						<div class="c-text-block__container o-container o-container-extra-narrow">
							<?php
							get_template_part( 'template-parts/components/call-to-action' );
							?>
						</div>
					</section>
				</div>
				<?php
				}
				?>
			</div>
		</div>
		</div>
		</div>
	</section>

<?php
get_footer();
