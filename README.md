AxE
===

Instalación
---------------

- Instalar la ultima versión de [Wordpress](https://wordpress.org/download/).
- Navegar hasta el directorio wp-content/
- Clonar el [repositorio](https://gitlab.com/ayarsava/axe)
- Navegar hasta el directorio wp-content/ y descomprimir el
  archivo [uploads.zip](https://gitlab.com/ayarsava/axe/-/blob/main/uploads.zip)

Base de datos
---------------

- Importar la base de datos enviada
- Desde PhpMyAdmin seleccionar la base de datos y en la pestaña SQL, pegar y ejecutar el siguiente texto:
    - `UPDATE wp_options SET option_value = replace(
      option_value, 'https://axe.ddev.site/', 'https://argentinosporlaeducacion.org/') WHERE option_name = 'home' OR option_name = 'siteurl';UPDATE wp_posts SET guid = replace(guid, 'https://axe.ddev.site/','https://argentinosporlaeducacion.org/');UPDATE wp_posts SET post_content = replace(post_content, '
      https://axe.ddev.site/', 'https://argentinosporlaeducacion.org/'); UPDATE wp_postmeta SET meta_value = replace(
      meta_value,'https://axe.ddev.site/','https://argentinosporlaeducacion.org/');
      `
- Repetir el proceso con el siguiente script:
    - `UPDATE wp_options SET option_value = replace(
      option_value, 'https://wordpress-639061-2187349.cloudwaysapps.com/', 'https://argentinosporlaeducacion.org/') WHERE option_name = 'home' OR option_name = 'siteurl';UPDATE wp_posts SET guid = replace(guid, 'https://wordpress-639061-2187349.cloudwaysapps.com/','https://argentinosporlaeducacion.org/');UPDATE wp_posts SET post_content = replace(post_content, '
      https://wordpress-639061-2187349.cloudwaysapps.com/', 'https://argentinosporlaeducacion.org/'); UPDATE wp_postmeta SET meta_value = replace(
      meta_value,'https://wordpress-639061-2187349.cloudwaysapps.com/','https://argentinosporlaeducacion.org/');
      `
